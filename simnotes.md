# Simulation notes

1. OMNET++ does not like a BSM to be deleted after use. Hence memory leaks imminent. This is in reference to attacks where BSMs are spoofed.
2. Attacker sends messages with random IDs between 0 and 100. Normal vehicle depends on sender ID to create estimates. Hence many new estimates are just left on the canvas.
3. When vehicles change lanes, KF is taking this into consideration and estimating the change in direction.
4. Normal vehicles stop in the simulation even though they receive messages from normal vehicles. This is because ``isDanger()` looks for if another vehicle is within safety distance and is ahead.

# Guidelines to not screw up building OMNET++ projects

1. Build your project as a shared library in makemake configurations instead of executable.
