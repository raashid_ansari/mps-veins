#! /bin/bash

cd /home/veins/workspace.omnetpp/mps-veins/simulations

config=""
if [[ $1 == "sudden" ]]; then
    config="SuddenAttack"
elif [[ $1 == "suddend" ]]; then
    config="SuddenAttackDetection"
elif [[ $1 == "constant" ]]; then
    config="ConstantAttacker"
elif [[ $1 == "constantd" ]]; then
    config="ConstantAttackerDetection"
elif [[ $1 == "commrange" ]]; then
    config="CommRangeBraking"
elif [[ $1 == "commranged" ]]; then
    config="CommRangeBrakingDetection"
elif [[ $1 == "noattack" ]]; then
    config="NoAttacks"
else
    echo "usage: runSimulation <sudden | suddend | constant | constantd | commrange | commranged | noattack>"
fi

../src/mps-veins -m -u Cmdenv -c $config -n .:../src:../../../src/veins-veins-4.5/examples:../../../src/veins-veins-4.5/src/veins --image-path=../../../src/veins-veins-4.5/images -l ../../../src/veins-veins-4.5/src/veins omnetpp.ini
