#! /usr/bin/env python
import pylab
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("filename", help="Name of the file to process")
args = parser.parse_args()

list_of_files = [[args.filename,"CPU %"]]

datalist = [ ( pylab.loadtxt(filename), label ) for filename, label in list_of_files ]

for data, label in datalist:
    pylab.plot( data[:,0], data[:,1])

font = {'family' : 'normal',
                'weight' : 'bold',
                        'size'   : 22}

font2 = {'family' : 'normal',
                'size'   : 18}

pylab.legend()
pylab.title("CPU load while running OMNET++ simulation", **font)
pylab.xlabel("Time (sec)", **font)
pylab.ylabel("CPU %", **font)
pylab.xticks(**font2)
pylab.yticks(**font2)
pylab.show()
