#! /usr/bin/env python3

# Script to plot data received from GhostCars simulations

from math import sqrt
import argparse
from matplotlib import pyplot
from matplotlib import ticker

# parse for CLI arguments
def parse_args():
    parser = argparse.ArgumentParser(description='Plot stuff using matplotlib')
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        help='Get verbose output')
    parser.add_argument('file_name', nargs='+', help='Name of the file to read from')
    return parser.parse_args()

# parse file input into list
def get_numbers(args, file_name):
    print('opening file: ', file_name) if args.verbose else None
    trace_fh = open(file_name, 'r')
    print('extracting data...') if args.verbose else None
    lines = trace_fh.readlines()
    print('data extracted: ', lines) if args.verbose else None

    # clean data
    print('cleaning data...') if args.verbose else None
    numbers = [None] * int(len(lines[20:]) / 2)
    for line_num, line in enumerate(lines[20:]):
        if line_num % 2 == 0:
            numbers[int(line_num/2)] = line.split()
        else:
            tmp_lines = line.split()
            for tmp_line in tmp_lines:
                numbers[int((line_num - 1)/2)].append(tmp_line)
    print("Numbers: ", numbers) if args.verbose else None

    # process data
    print('processing data...') if args.verbose else None
    time = [float(t[0]) for t in numbers]

    est_lat = [float(elat[1]) for elat in numbers]
    est_lon = [float(elon[2]) for elon in numbers]
    est_rms = [sqrt((float(tmp[1])**2 + float(tmp[2])**2)/2) for tmp in numbers]

    omnet_lat = [float(olat[3]) for olat in numbers]
    omnet_lon = [float(olon[4]) for olon in numbers]
    omnet_rms = [sqrt((float(tmp[3])**2 + float(tmp[4])**2)/2) for tmp in numbers]

    est_vs_meas_lat = [est - meas for est, meas in zip(est_lat, omnet_lat)]
    est_vs_meas_lon = [est - meas for est, meas in zip(est_lon, omnet_lon)]
    est_vs_meas_rms = [est - meas for est, meas in zip(est_rms, omnet_rms)]

    if args.verbose:
        print('Estimate vs Measured Latitude: ', est_vs_meas_lat)
        print('Estimate vs Measured Longitude: ', est_vs_meas_lon)
        print('Estimate vs Measured RMS: ', est_vs_meas_rms)

    gps_lat = [float(glat[5]) for glat in numbers]
    gps_lon = [float(glon[6]) for glon in numbers]
    gps_rms = [sqrt((float(tmp[5])**2 + float(tmp[6])**2)/2) for tmp in numbers]

    std_dev_x = [float(sdx[7]) for sdx in numbers]
    std_dev_y = [float(sdy[8]) for sdy in numbers]
    std_dev = [sqrt((float(tmp[7])**2 + float(tmp[8])**2)/2) for tmp in numbers]

    sender_speed_x = [float(spx[9]) for spx in numbers]
    sender_speed_y = [float(spy[10]) for spy in numbers]
    sender_speed_rms = [sqrt((float(tmp[9])**2 + float(tmp[10])**2)/2) for tmp in numbers]

    sender_distance = [float(sd[11]) for sd in numbers]
    sender_distance_std_dev = [float(sdsd[12]) for sdsd in numbers]

    # plot data
    print('plotting data...') if args.verbose else None
    fig, axes = pyplot.subplots(nrows=5, sharex=True)
    fig.suptitle('Trace ' + file_name)
    index = 0
    axes[index].plot(time, omnet_lat, linestyle='solid', color='orange', label='OMNET(lat)')
    axes[index].plot(time, omnet_lon, linestyle='solid', color='blue', label='OMNET(lon)')
    axes[index].plot(time, omnet_rms, linestyle='solid', color='green', label='OMNET(rms)')

    axes[index].plot(time, est_lat, linestyle='dotted', color='red', label='Estimate(lat)')
    axes[index].plot(time, est_lon, linestyle='dotted', color='cyan', label='Estimate(lon)')
    axes[index].plot(time, est_rms, linestyle='dotted', color='yellow', label='Estimate(rms)')
    index += 1

    axes[index].plot(time, est_vs_meas_lat, linestyle='dotted', color='orange', label='Prediction Error(lat)')
    axes[index].plot(time, est_vs_meas_lon, linestyle='dotted', color='blue', label='Prediction Error(lon)')
    axes[index].plot(time, est_vs_meas_rms, linestyle='dotted', color='green', label='Prediction Error(rms)')
    index += 1

    axes[index].plot(time, std_dev_x, linestyle='dashdot', color='orange', label='$\sigma$(lat)')
    axes[index].plot(time, std_dev_y, linestyle='dashdot', color='blue', label='$\sigma$(lon)')
    axes[index].plot(time, std_dev, linestyle='dashdot', color='green', label='$\sigma$(rms)')
    index += 1

    # axes[index].plot(time, gps_lat, linestyle='solid', color='orange', label='GPS(lat)')
    # axes[index].plot(time, gps_lon, linestyle='solid', color='blue', label='GPS(lon)')
    # axes[index].plot(time, gps_rms, linestyle='solid', color='green', label='GPS(rms)')
    # index += 1

    axes[index].plot(time, sender_speed_x, linestyle='solid', color='orange', label='Speed(x direction)')
    axes[index].plot(time, sender_speed_y, linestyle='solid', color='blue', label='Speed(y direction)')
    axes[index].plot(time, sender_speed_rms, linestyle='solid', color='green', label='Speed(rms)')
    index += 1

    axes[index].plot(time, sender_distance, linestyle='solid', color='orange', label='Sender Distance')
    axes[index].plot(time, sender_distance_std_dev, linestyle='dotted', color='orange', label='$\sigma$(Sender Distance)')

    for axis in axes:
        axis.grid(True)
        axis.legend(loc="right", bbox_to_anchor=(1.1, 0.5))
        axis.set_xlabel('time')
        axis.set_ylabel('Degrees')

    axes[index].set_ylabel('meters')
    pyplot.draw()

def main():
    args = parse_args()
    for f in args.file_name:
        get_numbers(args, f)
    pyplot.show()

if __name__ == '__main__':
    main()
