//#include <iostream>
//#include <Eigen/Dense>
//
//#include "KalmanFilter.h"
//
//
//int main() {
//    int n_states = 2;
//    int n_ctrl_states = 2;
//    int n_meas = 2;
//
//    double dt = 0.1;
//    Eigen::MatrixXd A(n_states, n_states); // System dynamics matrix
//    Eigen::MatrixXd B(n_states, n_ctrl_states); // Control dynamics matrix
//    Eigen::MatrixXd H(n_meas, n_states); // Output matrix
//    Eigen::MatrixXd Q(n_states, n_states); // Process noise covariance
//    Eigen::MatrixXd R(n_meas, n_meas); // Measurement noise covariance
//    Eigen::VectorXd co_vars_(n_states); // Covariances of x coordinates, y coordinates
//    Eigen::MatrixXd P(n_states, n_states); // Estimate error covariance
//    Eigen::VectorXd w_k(n_states); // Predicted state noise matrix
//
//    // Discrete LTI projectile motion, measuring position only
//    A << 1, 0,
//         0, 1;
//    B << dt, 0,
//         0, dt;
//    H = Eigen::MatrixXd::Identity(n_meas, n_states);
//
//    // Reasonable covariance matrices
////    Q << .05, .05, .0, .05, .05, .0, .0, .0, .0;
//    Q.setZero();
//    R = Eigen::MatrixXd::Constant(n_states, n_states, 5);
//    co_vars_ << 0.5, 0.4;
//    P = co_vars_ * co_vars_.transpose();
//    w_k.setZero();
//
//    // Construct the filter
//    KalmanFilter kf(dt, A, B, H, Q, R, P, w_k);
//
//    // // Best guess of initial states
//    // Eigen::VectorXd x0(n_states);
//    // Eigen::VectorXd u_k0(n_states);
//    // x0 << 0, 0;
//    // u_k0 << 10, 0;
//    // kf.init(0, x0, u_k0);
//
//    // std::cout << "Initial state of Kalman Filter: " << kf.state().transpose() << std::endl;
//
//    // // Feed measurements into filter, output next estimated state
//    // Eigen::VectorXd measurements(2);
//    // measurements << 1, 0;
//    // kf.update(measurements);
//
//    // // find the next 10 estimates (1st one using measurements and the rest without them)
//    // double t = 0;
//    // std::cout << "t = " << t << ", " << "measurements[0] = " << measurements.transpose()
//    //             << ", x_hat[0] = " << kf.state().transpose() << std::endl;
//    // for (unsigned int i = 1; i < 10; ++i) {
//    //     t += dt;
//    //     kf.update(measurements.setZero());
//    //     std::cout << "t = " << t << ", " << "measurements[" << i << "] = " << measurements.transpose()
//    //                 << ", x_hat[" << i << "] = " << kf.state().transpose() << std::endl;
//    // }
//    return 0;
//}
