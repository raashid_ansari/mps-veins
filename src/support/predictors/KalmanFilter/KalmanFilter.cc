/**
* Implementation of KalmanFilter class.
*
* @author: Hayk Martirosyan, Raashid Ansari
* @date: 04/06/2018 (mm/dd/yyyy)
*/

#include <exception>

#include "KalmanFilter.h"

KalmanFilter::KalmanFilter():
    initialized_(false) {}

KalmanFilter::~KalmanFilter() {}

void KalmanFilter::setXandI(Eigen::VectorXd& x0) {
    xHat_        = Eigen::VectorXd(stateVectorSize_);
    xHat_        = x0;
    I_           = Eigen::MatrixXd::Identity(stateVectorSize_, stateVectorSize_);
    initialized_ = true;
}

void KalmanFilter::init() {
    xHat_.setZero();
    initialized_ = true;
}

void KalmanFilter::step(const Eigen::VectorXd& u, const Eigen::VectorXd& z, const Eigen::VectorXd& v) {
    predict(u);
    update(z, v);
}

void KalmanFilter::update(const Eigen::VectorXd& z, const Eigen::VectorXd& v) {
    // update/Correction steps
    K_     = (P_ * H_.transpose()) * (H_ * P_ * H_.transpose() + R_).inverse();
    auto y = z + v - H_ * xHat_; // measurement to state relation function
    xHat_ += K_ * y;
    P_     = (I_ - K_ * H_) * P_;
}

/*
 * @brief Predict next state of the system
 * @author Raashid Ansari
 */
void KalmanFilter::predict(const Eigen::VectorXd& u) {
    if(!initialized_)
        throw std::runtime_error("Filter is not initialized!");

    // Prediction steps
    xHat_ = A_ * xHat_ + B_ * u + W_;
    P_    = A_ * P_ * A_.transpose() + Q_;
}

void KalmanFilter::setSizes(int stateVectorSize, int controlVectorSize, int measurementVectorSize) {
    this->stateVectorSize_       = stateVectorSize;
    this->controlVectorSize_     = controlVectorSize;
    this->measurementVectorSize_ = measurementVectorSize;
}

Eigen::VectorXd KalmanFilter::getState()                       { return xHat_; };
int             KalmanFilter::getStateVectorSize()             { return this->stateVectorSize_; }
int             KalmanFilter::getControlVectorSize()           { return this->controlVectorSize_; }
int             KalmanFilter::getMeasurementVectorSize()       { return this->measurementVectorSize_; }
void            KalmanFilter::setTimeStep(const double deltaT) { this->deltaT_ = deltaT; }
const double    KalmanFilter::getTimeStep()                    { return this->deltaT_; }
