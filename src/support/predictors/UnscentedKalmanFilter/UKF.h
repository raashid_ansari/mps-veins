

#ifndef SUPPORT_PREDICTORS_UNSCENTEDKALMANFITLER_H_
#define SUPPORT_PREDICTORS_UNSCENTEDKALMANFITLER_H_
#include <Eigen/Dense>

class UKF
{
public:
    /**
     * @brief Create a Kalman filter with the specified matrices.
     *   A - System dynamics matrix
     *   B - Control dynamics matrix
     *   H - Output matrix
     *   Q - Process noise covariance
     *   R - Measurement noise covariance
     *   P - Estimate error covariance
     */
    UKF();
    ~UKF();

    void         setTimeStep(const double deltaT);
    const double getTimeStep();
    virtual void setA       () = 0;
    virtual void setB       () = 0;
    virtual void setH       () = 0;
    virtual void setQ       () = 0;
    virtual void setR       () = 0;
    virtual void setP       () = 0;
    virtual void setW       () = 0;

    /**
     * @brief set sizes of matrices and vectors used in Kalman Filter
     * n = size of state vector
     * nu = size of input/control vector
     * m = size of measurement vector
     */
    void setSizes                (int stateVectorSize, int controlVectorSize, int measurementVectorSize);
    int  getStateVectorSize      ();
    int  getControlVectorSize    ();
    int  getMeasurementVectorSize();

    /**
     * @brief Initialize the filter with initial states as zero.
     */
    void init();

    /**
     * @brief Initialize the filter with initial values from
     * implementations using this library
     * @param x0 initial values of state variables
     */
    void setXandI(Eigen::VectorXd& x0);

    /**
     * @brief Update the estimated state based on measured values. The
     * time step is assumed to remain constant.
     * @param z measurement input vector
     * @param v measurement noise vector
     */
    void update(const Eigen::VectorXd& z, const Eigen::VectorXd& v);

    /**
    * @brief Predict new states
    * @param u control input
    */
    void predict(const Eigen::VectorXd& u);

    /**
     * @brief Run the Kalman Filter through one predict and update step to get next states
     * @param u control input vector
     * @param z measurement input vector
     * @param v measurement noise vector
     * @author Raashid Ansari
     */
    void step(const Eigen::VectorXd& u, const Eigen::VectorXd& z, const Eigen::VectorXd& v);

    /**
     * @brief Return the current state
     */
    Eigen::VectorXd getState();

    /**
     * Matrices for computation
     */
    Eigen::MatrixXd A_, B_, H_, Q_, R_, P_, K_;
    
    /**
     * Sigma point spreading parameter
     */
	double lambda_;
	
	///* predicted sigma points matrix
	Eigen::MatrixXd xSigPred_;
	
	///* Weights of sigma points
	Eigen::VectorXd weights_;
	
	
    /**
     * Error Matrices
     */
    Eigen::VectorXd W_;

private:
    /**
     * Is the filter initialized?
     */
    bool initialized_;

    /**
     * n-size identity
     */
    Eigen::MatrixXd I_;

    /**
     * Estimated states
     */
    Eigen::VectorXd xHat_;

    /** System dimensions
     * n_ = size of state vector
     * nu_ = size of input/control vector
     * m_ = size of measurement vector
     */
    int stateVectorSize_, controlVectorSize_, measurementVectorSize_;

    /**
     * Discrete time step
     */
    double deltaT_;
};

#endif /* SUPPORT_PREDICTORS_UNSCENTEDKALMANFILTER_H_ */
