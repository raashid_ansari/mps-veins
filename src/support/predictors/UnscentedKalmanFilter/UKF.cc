#include <exception>
#include "UKF.h"

UKF::UKF():
    initialized_(false) {}

UKF::~UKF() {}

void UKF::setXandI(Eigen::VectorXd& x0) {
    xHat_ = Eigen::VectorXd(stateVectorSize_);
    xHat_ = x0;
    I_ = Eigen::MatrixXd::Identity(stateVectorSize_, stateVectorSize_);
    initialized_ = true;
    xSigPred_ = Eigen::MatrixXd(stateVectorSize_, 2 * stateVectorSize_);
    weights_ = Eigen::VectorXd(2 * stateVectorSize_ + 1);
}

void UKF::init() {
    xHat_.setZero();
    initialized_ = true;
}

void UKF::step(const Eigen::VectorXd& u, const Eigen::VectorXd& z, const Eigen::VectorXd& v) {
    predict(u);
    update(z, v);
}

// @author: Cong Chen
void UKF::update(const Eigen::VectorXd& z, const Eigen::VectorXd& v) {
	auto Zsig = Eigen::MatrixXd(stateVectorSize_, 2 * stateVectorSize_ + 1);
	for (int i = 0; i < 2 * stateVectorSize_ + 1; i++)
	{
		Zsig.col(i) = H_ * xSigPred_.col(i);
	}
	
	  //mean predicted measurement
    auto z_pred = Eigen::VectorXd(stateVectorSize_);
    z_pred.fill(0.0);
    for (int i = 0; i < 2 * stateVectorSize_ + 1; i++) {
      z_pred = z_pred + weights_(i) * Zsig.col(i);
    }

	  //measurement covariance matrix S
	auto S = Eigen::MatrixXd(stateVectorSize_, stateVectorSize_);
	S.fill(0.0);
	for (int i = 0; i < 2 * stateVectorSize_ + 1; i++) {  //2n+1 simga points

		//residual
		auto z_diff = Zsig.col(i) - z_pred;

		S = S + weights_(i) * z_diff * z_diff.transpose();
	}
	  
	S = S + R_;
	
		  //create matrix for cross correlation Tc
	  auto Tc = Eigen::MatrixXd(stateVectorSize_, stateVectorSize_);

	  /*****************************************************************************
	  *  UKF  Measurement Update
	  ****************************************************************************/
	  //calculate cross correlation matrix
	  Tc.fill(0.0);
	  for (int i = 0; i < 2 * stateVectorSize_ + 1; i++) {  //2n+1 simga points

		//residual
		auto z_diff = Zsig.col(i) - z_pred;

		// state difference
		auto x_diff = xSigPred_.col(i) - xHat_;

		Tc = Tc + weights_(i) * x_diff * z_diff.transpose();
	  }

	//Kalman gain K;
	auto K = Tc * S.inverse();

	//residual
	auto z_diff = z - z_pred;


	//update state mean and covariance matrix
	xHat_ = xHat_ + K * z_diff;
	P_ = P_ - K*S*K.transpose();
  
}

/*
 * @brief Predict next state of the system
 * @author Cong Chen
 */
void UKF::predict(const Eigen::VectorXd& u) {
    if(!initialized_)
        throw std::runtime_error("Filter is not initialized!");
	
	/*  Generate Sigma Points
	****************************************************************************/
	//create sigma point matrix
	auto Xsig = Eigen::MatrixXd(stateVectorSize_, 2 * stateVectorSize_ + 1);
	
	//calculate square root of P
	auto rP = static_cast<Eigen::MatrixXd>(P_.llt().matrixL());
	
	//set lambda for non-augmented sigma points
	lambda_ = 3 - stateVectorSize_;
	
	//set first column of sigma point matrix
	Xsig.col(0) = xHat_;
	
	//set remaining sigma points
	for (int i = 0; i < stateVectorSize_; i++)
	{
		Xsig.col(i + 1) = xHat_ + sqrt(lambda_ + stateVectorSize_) * rP.col(i);
		Xsig.col(i + 1 + stateVectorSize_) = xHat_ - sqrt(lambda_ + stateVectorSize_) * rP.col(i);
	}

	for(int i = 0; i < 2 * stateVectorSize_ + 1; i++)
	{
		xSigPred_.col(i) = A_ * Xsig.col(i) + B_ * u + W_;
	}
	
	/*****************************************************************************
	*  Convert Predicted Sigma Points to Mean/Covariance
	****************************************************************************/
	
	// set weights
	auto weight_0 = lambda_ / (lambda_ + stateVectorSize_);
	weights_(0) = weight_0;
	for (int i = 1; i < 2 * stateVectorSize_ + 1; i++) {  //2n+1 weights
		double weight = 0.5 / (stateVectorSize_ + lambda_);
		weights_(i) = weight;
	}
	
	//predicted state mean
	xHat_.fill(0.0);             //******* necessary? *********
	for (int i = 0; i < 2 * stateVectorSize_ + 1; i++) {  //iterate over sigma points
		xHat_ = xHat_ + weights_(i) * xSigPred_.col(i);
	}
	
	//predicted state covariance matrix
	P_.fill(0.0);             //******* necessary? *********
	for (int i = 0; i < 2 * stateVectorSize_ + 1; i++) {  //iterate over sigma points
		
		// state difference
		auto x_diff = xSigPred_.col(i) - xHat_;

		
		P_ = P_ + weights_(i) * x_diff * x_diff.transpose();
	}

    P_ = P_ + Q_;
}

void UKF::setSizes(int stateVectorSize, int controlVectorSize, int measurementVectorSize) {
    this->stateVectorSize_       = stateVectorSize;
    this->controlVectorSize_     = controlVectorSize;
    this->measurementVectorSize_ = measurementVectorSize;
}

Eigen::VectorXd UKF::getState()                       { return xHat_; };
int             UKF::getStateVectorSize()             { return this->stateVectorSize_; }
int             UKF::getControlVectorSize()           { return this->controlVectorSize_; }
int             UKF::getMeasurementVectorSize()       { return this->measurementVectorSize_; }
void            UKF::setTimeStep(const double deltaT) { this->deltaT_ = deltaT; }
const double    UKF::getTimeStep()                    { return this->deltaT_; }
