/*
 * PlausibilityChecks.h
 *
 *  Created on: Jun 17, 2018
 *      Author: veins
 */

#ifndef SUPPORT_PLAUSIBILITYCHECKS_H_
#define SUPPORT_PLAUSIBILITYCHECKS_H_

#include "veins/base/utils/Coord.h"

typedef struct {
    int       id;
    Coord     speed;
    Coord     position;
    Coord     acceleration;
    bool      isDiscarded;
    int       estimateVsMeasureRating;
    int       artCheckRating;
    int       mdmCheckRating;
    int       suspicionRating; // increase this count for each Plausibility check failed
    Coord     firstPos;
    simtime_t lastBeaconTime;
} Sender_t;

class PlausibilityChecks
{
public:
    PlausibilityChecks();
    ~PlausibilityChecks();
    const bool acceptanceRangeThresholdCheck(
            const Coord& bsmPos,
            const Coord& receiverPos,
            const double commRange);

    void minimumDistanceMovedCheck(
            std::vector<Sender_t>& senderVector,
            const double beaconTimeThreshold,
            const double commRange,
            const int multiplier);
};

#endif /* SUPPORT_PLAUSIBILITYCHECKS_H_ */
