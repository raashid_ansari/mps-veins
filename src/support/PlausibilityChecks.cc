/*
 * PlausibilityChecks.cc
 *
 *  Created on: Jun 17, 2018
 *      Author: Raashid Ansari
 */

#include "PlausibilityChecks.h"

PlausibilityChecks::PlausibilityChecks() {}
PlausibilityChecks::~PlausibilityChecks() {}



const bool PlausibilityChecks::
acceptanceRangeThresholdCheck(
        const Coord& bsmPos,
        const Coord& receiverPos,
        const double commRange) {

    // get Euclidean distance between BSM and receiver
    const auto DISTANCE = bsmPos.distance(receiverPos);

    // For Standard Error (Confidence Interval) assume:
    // mu    = 300 meters
    // sigma = 15 meters
    // CI    = 95%
    const auto STD_ERR = 18.59;

    // Check if greater than communication range +- gaussianThreshold
    return DISTANCE > commRange - STD_ERR;
}



void PlausibilityChecks::
minimumDistanceMovedCheck(
        std::vector<Sender_t>& senderVector,
        const double beaconTimeThreshold,
        const double commRange,
        const int multiplier) {

    const auto D_MDM     = multiplier * commRange;

    for (auto& sender : senderVector) {
        if (sender.lastBeaconTime - simTime() > beaconTimeThreshold) {
            auto distance = sender.position.distance(sender.firstPos);
            if (distance < D_MDM)
                sender.mdmCheckRating++;
        }
    }
}
