//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "BaseTrafficManager.h"

Define_Module(BaseTrafficManager);

bool BaseTrafficManager::randomize_ = false;

BaseTrafficManager::BaseTrafficManager() {}
BaseTrafficManager::~BaseTrafficManager() {}

void BaseTrafficManager::initialize(int stage) {
    cSimpleModule::initialize(stage);
#ifdef DEBUG
     std::cout << "BaseTrafficManager: init start stage " << stage << std::endl;
#endif
    if (stage == 0) {
        initScenario_ = false;
        laneIds_.clear();
        junctionIds_.clear();
        routeIds_.clear();
        vehTypeIds_.clear();

        traciScenarioManager_ = FindModule<Veins::TraCIScenarioManager*>::findGlobalModule();
        traciCmdInterface_ = traciScenarioManager_->getCommandInterface();
    }
#ifdef DEBUG
     std::cout << "BaseTrafficManager: init done" << std::endl;
#endif
}

void BaseTrafficManager::handleMessage(cMessage *msg) {
//    if (msg == addVehicleTrigger) {
//        BaseTrafficManager::createVehicle();
//    }
}

void BaseTrafficManager::loadTrafficInfo() {
#ifdef DEBUG
     std::cout << "BaseTrafficManager: loadTrafficInfo start" << std::endl;
#endif
    traciCmdInterface_ = FindModule<Veins::TraCIScenarioManager*>::findGlobalModule()->getCommandInterface();

    if (roadIds_.size() == 0) { // Use condition road_ids.size() != 0 and throw error if not empty, Do this for all following conditions.
#ifdef DEBUG
         std::cout << "BaseTrafficManager: load road_ids start" << std::endl;
#endif
        auto road_list = traciCmdInterface_->getRoadIds();
        for (auto& road_iter : road_list) {
            EV << "Got road: " << road_iter << std::endl;
            roadIds_.insert(std::pair<std::string,int>(road_iter, 0));
        }
#ifdef DEBUG
         std::cout << "BaseTrafficManager: load road_ids end" << std::endl;
#endif
    }

    if (laneIds_.size() == 0) {
        auto lane_list = traciCmdInterface_->getLaneIds();
        for (auto& lane_iter : lane_list) {
            EV << "Adding lane: " << lane_iter << std::endl;
            laneIds_.push_back(lane_iter);
            auto road_id = traciCmdInterface_->lane(lane_iter).getRoadId();
            roadIds_.find(road_id)->second++;
            roadIdOfLane_[road_id].push_back(lane_iter);
        }
    }

    if (routeIds_.size() == 0) {
        auto route_list = traciCmdInterface_->getRouteIds();
        for (auto& route_iter : route_list) {
            EV << "Adding route: " << route_iter << std::endl;
            routeIds_.push_back(route_iter);
            auto roads_in_route = traciCmdInterface_->route(route_iter).getRoadIds();
            auto first_road_in_route = roads_in_route.begin();
            routeStartLaneIds_[route_iter] = roadIdOfLane_[*first_road_in_route];
        }
    }

    if (junctionIds_.size() == 0) {
        auto junction_list = traciCmdInterface_->getJunctionIds();
        for (auto& junc_iter : junction_list) {
            EV << "Adding junction: " << junc_iter << std::endl;
            junctionIds_.push_back(junc_iter);
        }
    }

    if (vehTypeIds_.size() == 0) {
        auto veh_type_list = traciCmdInterface_->getVehicleTypeIds();
        for (auto& veh_type_iter : veh_type_list) {
            if(veh_type_iter != "DEFAULT_PEDTYPE") {
                EV << "Adding vehicle type: " << veh_type_iter << std::endl;
                vehTypeIds_.push_back(veh_type_iter);
            }
        }
    }
#ifdef DEBUG
     std::cout << "BaseTrafficManager: loadTrafficInfo end" << std::endl;
#endif
}

void BaseTrafficManager::createVehicle(std::string veh_id) {
#ifdef DEBUG
     std::cout << "BaseTrafficManager: createVehicle start" << std::endl;
#endif
    if (!initScenario_) {
#ifdef DEBUG
         std::cout << "BaseTrafficManager: call loadTrafficInfo" << std::endl;
#endif
        BaseTrafficManager::loadTrafficInfo();
        initScenario_ = true;
    }

#ifdef DEBUG
    for (auto& i : roadIds_)
        std::cout << "road id: " << i.first << " num of lanes: " << i.second << std::endl;
#endif

    std::srand(std::time(0));
    auto route_index = this->randomize_ ? std::rand()%routeIds_.size() : 1;
#ifdef DEBUG
    std::cout << "BaseTrafficManager: route_index=" << route_index << " lane_index=" << static_cast<int>(lane_index) << "" << std::endl;
#endif

#ifdef DEBUG
    std::cout << "BaseTrafficManager: create Vehicle obj" << std::endl;
#endif
    auto veh_type = veh_id.substr(0, 1) == "n" ? vehTypeIds_[1] : vehTypeIds_[0];
    BaseTrafficManager::Vehicle v(veh_id, veh_type, 0, 1.0, 2.0);
#ifdef DEBUG
    std::cout << "VEHICLE TYPE: " << veh_type << std::endl;
    std::cout << "BaseTrafficManager: get route_id" << std::endl;
#endif
    auto route_id = routeIds_[route_index];

#ifdef DEBUG
    std::cout << "BaseTrafficManager: inserting vehicle in traci" << std::endl;
    EV << "adding vehicle " << v.getId() << " of type " << v.getType() << " on route " << route_id << std::endl;
#endif
    // The following method, addVehicle(), returns a boolean value indicating
    // whether a vehicle insertion was successful. It may be useful to store
    // that value in a variable for debugging later on.
    traciCmdInterface_->addVehicle(v.getId(), v.getType(), route_id, -traciCmdInterface_->DEPART_NOW, v.getPosition(), v.getSpeed());
#ifdef DEBUG
    std::cout << "Vehicle insertion ";
    r ? std::cout << "SUCCESS" : std::cout << "FAILURE";
    std::cout << " at " << simTime() << std::endl;
    std::cout << "BaseTrafficManager: createVehicle end" << std::endl;
#endif
}

void BaseTrafficManager::finish() {
#ifdef DEBUG
    std::cout << "BaseTrafficManager: finished" << std::endl;
#endif
}

void               BaseTrafficManager::setRandomize        (bool val)                      { this->randomize_ = val; }

const std::string& BaseTrafficManager::Vehicle::getId      ()                        const { return this->id_; }
void               BaseTrafficManager::Vehicle::setId      (const std::string& id)         { this->id_ = id; }

const int8_t       BaseTrafficManager::Vehicle::getLane    ()                        const { return this->lane_; }
void               BaseTrafficManager::Vehicle::setLane    (int8_t lane)                   {this->lane_ = lane;}

const double       BaseTrafficManager::Vehicle::getPosition()                        const { return this->position_; }
void               BaseTrafficManager::Vehicle::setPosition(double position)               { this->position_ = position; }

const double       BaseTrafficManager::Vehicle::getSpeed   ()                        const { return this->speed_; }
void               BaseTrafficManager::Vehicle::setSpeed   (double speed)                  { this->speed_ = speed; }

const std::string& BaseTrafficManager::Vehicle::getType    ()                        const { return this->type_; }
void               BaseTrafficManager::Vehicle::setType    (const std::string& type)       { this->type_ = type; }
