//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __GHOSTCARS_BASETRAFFICMANAGER_H_
#define __GHOSTCARS_BASETRAFFICMANAGER_H_

#include <omnetpp/csimplemodule.h>
#include <stdint.h>
#include <list>
#include <map>
#include <string>
#include <vector>

#include "veins/modules/mobility/traci/TraCIScenarioManager.h"
#include "veins/modules/mobility/traci/TraCICommandInterface.h"

/**
 * Class to help control vehicles in a scenario
 */
class BaseTrafficManager : public omnetpp::cSimpleModule
{
public:
    ~BaseTrafficManager();
    BaseTrafficManager();
    virtual void initialize(int stage);
    virtual void finish();
    virtual void createVehicle(std::string veh_id);
    void         setRandomize(bool val);

protected:
//    typedef std::list<std::string> StringList;
    typedef std::vector<std::string>              StringVector_t;
    typedef std::map<std::string, StringVector_t> StringStringVectorMap_t;
    std::map<std::string, int>    roadIds_; // key=road_id, value=number_of_lanes
    StringVector_t                laneIds_;
    StringVector_t                junctionIds_;
    StringVector_t                routeIds_;
    StringVector_t                vehTypeIds_;
    StringStringVectorMap_t       roadIdOfLane_;
    StringStringVectorMap_t       routeStartLaneIds_;
    Veins::TraCIScenarioManager*  traciScenarioManager_;
    Veins::TraCICommandInterface* traciCmdInterface_;
    bool                          initScenario_;
    static bool                   randomize_;

protected:
    virtual void handleMessage(omnetpp::cMessage *msg);
    void loadTrafficInfo();
    class Vehicle
    {
    public:
        Vehicle();
        Vehicle(std::string id, std::string type, int8_t lane, double position, double speed):
            id_(id),
            type_(type),
            lane_(lane),
            position_(position),
            speed_(speed)
        {}
        const std::string& getId      () const;
        void               setId      (const std::string& id);

        const int8_t       getLane    () const;
        void               setLane    (int8_t lane);

        const double       getPosition() const;
        void               setPosition(double position);

        const double       getSpeed   () const;
        void               setSpeed   (double speed);

        const std::string& getType    () const;
        void               setType    (const std::string& type);

    protected:
        std::string id_;
        std::string type_;
        int8_t      lane_;
        double      position_;
        double      speed_;
    }; // class Vehicle
}; // class BaseTrafficManager

#endif
