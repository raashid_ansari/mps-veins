//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "TrafficManager.h"

Define_Module(TrafficManager);

inline simtime_t getRandTime() {
    return fmod(std::rand(), 2.0) + 1;
}

TrafficManager::TrafficManager() {}
TrafficManager::~TrafficManager() {}

void TrafficManager::initialize(int stage) {
#ifdef DEBUG
    std::cout << "Initializing Traffic Manager" << std::endl;
#endif
    BaseTrafficManager::initialize(stage);
    if (stage == 0) {
        numAttackerVeh_    = par("numAttackerVeh").longValue();
        numNormalVeh_      = par("numNormalVeh").longValue();
#ifdef DEBUG
        std::cout << "Number of normal vehicles: " << num_of_normal_veh_ << std::endl;
#endif
        ghostsPerAttacker_ = par("ghostsPerAttacker").longValue();
        std::srand(time(0));

        addCar_ = new cMessage("addCar");

        BaseTrafficManager::setRandomize(par("randomize").boolValue());

        scheduleAt(simTime() + traciScenarioManager_->getUpdateInterval() + getRandTime(), addCar_);
    }
#ifdef DEBUG
    std::cout << "TrafficManager: init done" << std::endl;
#endif
}

void TrafficManager::handleMessage(cMessage *msg) {
#ifdef DEBUG
     std::cout << "TrafficManager: handleMessage start" << std::endl;
#endif
    if (msg == addCar_) {
#ifdef DEBUG
          std::cout << "Number of Vehicles: " << numNormalVeh_ << std::endl;
#endif
        TrafficManager::createCars();
    } else {
        omnetpp::cSimpleModule::error("Traffic Manager received unknown Message: %s", msg->getName());
    }
#ifdef DEBUG
     std::cout << "TrafficManager: handleMessage end" << std::endl;
#endif
}

void TrafficManager::createCars() {
    auto kCarCreateOrder = par("carCreateOrder").longValue();
    srand(time(NULL));
    switch(kCarCreateOrder) {
    case ATTACKER_CAR_FIRST: {
#ifdef DEBUG
        std::cout << "Attacker car first creation order" << std::endl;
#endif
        if (numAttackerVeh_-- > 0) {
            auto new_attacker_car = new cars::Car("attacker");
            carVector_.push_back(new_attacker_car);
            scheduleAt(simTime() + traciScenarioManager_->getUpdateInterval() + getRandTime(), addCar_);
        } else if (numNormalVeh_-- > 0) {
            auto new_normal_car = new cars::Car("normal");
            carVector_.push_back(new_normal_car);
            scheduleAt(simTime() + traciScenarioManager_->getUpdateInterval() + getRandTime(), addCar_);
        }
        break;
    }
    case NORMAL_CAR_FIRST: {
#ifdef DEBUG
        std::cout << "Normal car first creation order" << std::endl;
#endif
        if (numNormalVeh_-- > 0) {
            auto new_normal_car = new cars::Car("normal");
            carVector_.push_back(new_normal_car);
            scheduleAt(simTime() + traciScenarioManager_->getUpdateInterval() + getRandTime(), addCar_);
        } else if (numAttackerVeh_-- > 0) {
            auto new_attacker_car = new cars::Car("attacker");
            carVector_.push_back(new_attacker_car);
            scheduleAt(simTime() + traciScenarioManager_->getUpdateInterval() + getRandTime(), addCar_);
        }
        break;
    }
    case RANDOM_SELECTION: {
#ifdef DEBUG
        std::cout << "Random car creation order" << std::endl;
#endif
        auto which_car = -1;
        if (numNormalVeh_ + numAttackerVeh_ > 0)
            which_car = rand() % 2; // Unit test here to check if value is only between 0 or 1
        if (which_car == -1)
            omnetpp::cSimpleModule::error("Invalid car option OR all cars already created.");
        if (which_car == 0 && numNormalVeh_-- > 0) {
            auto new_normal_car = new cars::Car("normal");
            carVector_.push_back(new_normal_car);
        } else if (which_car == 1 && numAttackerVeh_-- > 0) {
            auto new_attacker_car = new cars::Car("attacker");
            carVector_.push_back(new_attacker_car);
        }
        if (numNormalVeh_ + numAttackerVeh_ > 0)
            scheduleAt(simTime() + traciScenarioManager_->getUpdateInterval() + getRandTime(), addCar_);
        break;
    }
    default:
        omnetpp::cSimpleModule::error("Invalid input for car creation order.");
        break;
    }
}

void TrafficManager::finish() {
#ifdef DEBUG
    std::cout << "TrafficManager: finish start" << std::endl;
    for(auto& it : vec_veh_map_)
        std::cout << "Vehicle Name: " << it->getType() << "\t" << "Vehicle simulation ID: " << it->getSimulationId() << std::endl;
#endif

    if (addCar_->isScheduled()) {
        omnetpp::cSimpleModule::cancelAndDelete(addCar_);
        addCar_ = 0;
    } else if (addCar_) {
        delete(addCar_);
    }

    carVector_.clear();
    BaseTrafficManager::finish();
#ifdef DEBUG
     std::cout << "TrafficManager: finish end" << std::endl;
#endif
}

const std::vector<cars::Car*>& TrafficManager::getCarVector()           const { return this->carVector_; }
const long                     TrafficManager::getGhostCarPerAttacker() const { return this->ghostsPerAttacker_; }
