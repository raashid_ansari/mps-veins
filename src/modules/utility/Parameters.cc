//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "Parameters.h"

Parameters::Parameters() {}

Parameters::Parameters(Coord position, Coord speed, Coord acceleration):
        position_(position),
        speed_(speed),
        acceleration_(acceleration) {}

Parameters::~Parameters() {}

const Coord& Parameters::getPosition()                              const { return this->position_; }
void         Parameters::setPosition(const Coord& position)               { position_ = position; }

const Coord& Parameters::getSpeed()                                 const { return this->speed_; }
void         Parameters::setSpeed(const Coord& speed)                     { speed_ = speed; }

const Coord& Parameters::getAcceleration()                          const { return this->acceleration_; }
void         Parameters::setAcceleration(const Coord& acceleration)       { acceleration_ = acceleration; }
