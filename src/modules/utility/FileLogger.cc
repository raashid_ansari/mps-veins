/*
 * FileLogger.cc
 *
 *  Created on: Oct 31, 2017
 *      Author: veins
 */

#include <fstream>
#include <sstream>
#include <iostream>

#include "FileLogger.h"

FileLogger::FileLogger() {}

FileLogger::~FileLogger() {}

void FileLogger::log(std::string fileName, std::string content) {
    auto ostream = std::ostringstream{};
    ostream << fileName << ".dat";

    auto fh = std::ofstream(ostream.str(), std::ios::out | std::ios::app);
    if (!fh.is_open())
        std::cerr << "Could not open the file: " << ostream.str() << std::endl;

    fh << content << "\n";
    fh.close();
}
