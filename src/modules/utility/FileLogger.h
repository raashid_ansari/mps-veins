/*
 * FileLogger.h
 *
 *  Created on: Oct 31, 2017
 *      Author: veins
 */

#ifndef UTILS_FILELOGGER_H_
#define UTILS_FILELOGGER_H_

class FileLogger
{
public:
    FileLogger         ();
    virtual ~FileLogger();

    void log           (std::string fileName, std::string content);
};

#endif /* UTILS_FILELOGGER_H_ */
