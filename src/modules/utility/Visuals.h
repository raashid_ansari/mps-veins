/*
 * Visuals.h
 *
 *  Created on: Aug 24, 2017
 *      Author: veins
 */

#ifndef MODULES_UTILITY_VISUALS_H_
#define MODULES_UTILITY_VISUALS_H_

#include "messages/BasicSafetyMessageExtended_m.h"
#include "modules/vehicle/Car.h"

#include <Eigen/Dense>

class Visuals {
public:
    Visuals                ();
    Visuals                (const cars::Car* car);
    virtual ~Visuals       ();
    void showBSMBubble     (const int senderId) const;
    void updateRing        (const int radius, const std::string& color) const;
    void updateGhostVisuals(const BasicSafetyMessageExtended& bsm, const BasicSafetyMessageExtended& spoofedBsm, cCanvas& canvas);
    void plotPredictions   (const int numPreds, const int senderId, cCanvas& canvas, const std::vector<Eigen::VectorXd> predictions);
private:
    const cars::Car* car_;
};

#endif /* MODULES_UTILITY_VISUALS_H_ */
