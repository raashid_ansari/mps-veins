/*
 * Visuals.cc
 *
 *  Created on: Aug 24, 2017
 *      Author: veins
 */

#include "Visuals.h"
#include "modules/vehicle/GhostVehicle.h"

Visuals::Visuals() {}

Visuals::Visuals(const cars::Car* car)
    : car_(car) {}

Visuals::~Visuals() {}

void Visuals::showBSMBubble(const int senderId) const {
    auto ss = std::ostringstream{};
    ss << "BSM from " << senderId;
    car_->getSimulationModule()->bubble(ss.str().c_str());
}

void Visuals::updateRing(const int radius, const std::string& color) const {
    auto ss = std::ostringstream{};
    ss << "r=" << radius << "," << color;
    car_->getSimulationModule()->getDisplayString().updateWith(ss.str().c_str());
}

void Visuals::updateGhostVisuals(const BasicSafetyMessageExtended& bsm, const BasicSafetyMessageExtended& spoofedBsm, cCanvas& canvas) {
    auto ss = std::ostringstream{};
    ss << "ghost_" << car_->getSimulationId() << "_" << bsm.getSenderAddress();
    auto newGhostPos = cFigure::Point(spoofedBsm.getSenderPos().x, spoofedBsm.getSenderPos().y);

    if (canvas.getFigure(ss.str().c_str()) != nullptr) { // if ghost already present, get ghost's figure, update its position
        auto ghost = check_and_cast<cRectangleFigure*>(canvas.getFigure(ss.str().c_str()));
        ghost->setPosition(newGhostPos, cFigure::ANCHOR_CENTER);
    }
    else if (bsm.getWsmData() != std::string("ghost")) { // if ghost's figure not present, create ghost figure, update position, add to the canvas
        auto ghostVehObj = cars::GhostVehicle{};
        ghostVehObj.createVehicle(canvas, ss.str());
        ghostVehObj.updatePosition(newGhostPos);
    }
}

void Visuals::plotPredictions(const int num_preds, const int senderId, cCanvas& canvas, const std::vector<Eigen::VectorXd> predictions) {
    for (auto i = 0; i < num_preds; ++i) {
        auto ss = std::ostringstream{};
        ss << "estimate_" << senderId << "_" << i;

        if (canvas.getFigure(ss.str().c_str()) != nullptr) { // if figure already present, get figure, update position
            auto estimate = check_and_cast<cRectangleFigure*>(canvas.getFigure(ss.str().c_str()));
            estimate->setPosition(cFigure::Point(predictions.at(i)[0], predictions.at(i)[1]), cFigure::ANCHOR_CENTER);
        }
        else { // if figure not present, create figure, fill figure with color, update position, and add to the canvas
            auto estimate = new cRectangleFigure(ss.str().c_str());
            estimate->setBounds(cFigure::Rectangle(0, 0, 2, 2));
            cFigure::Color color = (i == 0) ? cFigure::GREEN : cFigure::RED;
            estimate->setFillColor(color);
            estimate->setFilled(true);
            estimate->setFillOpacity(0.5);
            estimate->setPosition(cFigure::Point(predictions.at(i)[0], predictions.at(i)[1]), cFigure::ANCHOR_CENTER);
            canvas.addFigure(estimate);
        }
    }
}
