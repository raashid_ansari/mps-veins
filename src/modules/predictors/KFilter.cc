//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "KFilter.h"

KFilter::KFilter() {}
KFilter::~KFilter() {}

void KFilter::setA() {
    // State transition Matrix
    auto deltaT          = getTimeStep();
    auto stateVectorSize = getStateVectorSize();
    A_                   = Eigen::MatrixXd(stateVectorSize, stateVectorSize); // System dynamics matrix

    A_ << 1, 0, deltaT, 0,
          0, 1, 0,      deltaT,
          0, 0, 1,      0,
          0, 0, 0,      1;
}

void KFilter::setB() {
    auto deltaT = getTimeStep();
    auto stateVectorSize  = getStateVectorSize();
    auto controlVectorSize = getControlVectorSize();
    // Input Control Matrix (B_ = Eigen::MatrixXd::Identity(rows=n_states, cols=n_ctrl_states))
    B_ = Eigen::MatrixXd(stateVectorSize, controlVectorSize); // Control dynamics matrix
    B_ << 0.5*deltaT*deltaT, 0,
          0                , 0.5*deltaT*deltaT,
          deltaT           , 0,
          0                , deltaT;
//    B_.setZero();
}

void KFilter::setH() {
    auto stateVectorSize = getStateVectorSize();
    auto measurementVectorSize = getMeasurementVectorSize();
    // Measurement Output Matrix
    H_ = Eigen::MatrixXd(measurementVectorSize, stateVectorSize); // Output matrix
//    H << 1, 0, 0, 0,
//         0, 1, 0, 0,
//         0, 0, 1, 0,
//         0, 0, 0, 1;
    H_ = Eigen::MatrixXd::Identity(measurementVectorSize, stateVectorSize);
}

void KFilter::setQ() {
    auto stateVectorSize = getStateVectorSize();
    // Action Uncertainty
    Q_ = Eigen::MatrixXd(stateVectorSize, stateVectorSize); // Process noise covariance
    Q_ = Eigen::MatrixXd::Identity(stateVectorSize, stateVectorSize) * 0.00001;
//    Q_ << 0.00001, 0,       0,       0,
//         0,       0.00001, 0,       0,
//         0,       0,       0.00001, 0,
//         0,       0,       0,       0.00001;
}

void KFilter::setR() {
    auto measurementVectorSize = getMeasurementVectorSize();
    // Measurement Noise (R_ = Eigen::MatrixXd::Identity(n_states, n_ctrl_states) * 0.1)
    R_ = Eigen::MatrixXd(measurementVectorSize, measurementVectorSize); // Measurement noise covariance
    R_ = Eigen::MatrixXd::Identity(measurementVectorSize, measurementVectorSize) * 0.1;
//    R << 0.1, 0  , 0  , 0,
//         0  , 0.1, 0  , 0,
//         0  , 0  , 0.1, 0,
//         0  , 0  , 0  , 0.1;
}

void KFilter::setP() {
    auto stateVectorSize = getStateVectorSize();
    // Prediction Error (P_ = Eigen::MatrixXd::Identity(n_states, n_states) * 0.25)
    P_ = Eigen::MatrixXd(stateVectorSize, stateVectorSize); // Estimate error covariance
    P_ = Eigen::MatrixXd::Identity(stateVectorSize, stateVectorSize) * 1000.0;
//    P << 0.25, 0   , 0   , 0,
//         0   , 0.25, 0   , 0,
//         0   , 0   , 0.25, 0,
//         0   , 0   , 0   , 0.25;
}

void KFilter::setW() {
    auto stateVectorSize = getStateVectorSize();
//  Prediction Noise (W_ = Eigen::VectorXd::Constant(n_states, 0) -OR- W_.setZero())
    W_ = Eigen::VectorXd::Constant(stateVectorSize, 0);
    W_ = Eigen::VectorXd(stateVectorSize); // Predicted state noise matrix
//    W << 0,
//         0,
//         0,
//         0;
}

void KFilter::init(const double deltaT, const Parameters parameters) {
    const auto ESTIMATED_VALUES = 4;
    const auto CONTROL_INPUTS   = 2;
    const auto MEASURED_VALUES  = 4;
    setSizes(ESTIMATED_VALUES, CONTROL_INPUTS, MEASURED_VALUES);
    setTimeStep(deltaT);
    setA();
    setB();
    setH();
    setR();
    setQ();
    setP();
    setW();

    // Initial states
    auto x0 = Eigen::VectorXd(getStateVectorSize());
    x0 << parameters.getPosition().x, parameters.getPosition().y, parameters.getSpeed().x, parameters.getSpeed().y;

    setXandI(x0);
}
