/*
 * GhostVehicle.cc
 *
 *  Created on: Jun 6, 2017
 *      Author: veins
 */

#include "GhostVehicle.h"

int cars::GhostVehicle::ghostVehCount_ = 0;

//    Create Ghost vehicle figure
cars::GhostVehicle::GhostVehicle() {
//    std::cout << "$$$$$$$$$ Created " << Cars::GhostVehicle::getGivenId() << " in simulation." << std::endl;
}

cars::GhostVehicle::~GhostVehicle() {}

void cars::GhostVehicle::createVehicle(omnetpp::cCanvas& canvas, const std::string ss) {
    ghostVehFigure_ = new omnetpp::cRectangleFigure(ss.c_str());
    ghostVehFigure_->setBounds(omnetpp::cFigure::Rectangle(0, 0, 5, 5));
    ghostVehFigure_->setFillColor(omnetpp::cFigure::GREY);
    ghostVehFigure_->setFilled(true);
    ghostVehFigure_->setFillOpacity(0.5);
    ghostVehFigure_->setPosition(omnetpp::cFigure::Point(0, 0), omnetpp::cFigure::ANCHOR_CENTER);
    canvas.addFigure(ghostVehFigure_);
    ghostVehCount_++;
}

void cars::GhostVehicle::updatePosition(const omnetpp::cFigure::Point position) const {
    ghostVehFigure_->setPosition(position, omnetpp::cFigure::ANCHOR_CENTER);
}

void cars::GhostVehicle::removeVehicle(omnetpp::cCanvas& canvas, std::string ss) {
    if (canvas.getFigure(ss.c_str()) != nullptr) {
        auto ghost = canvas.getFigure(ss.c_str());
        canvas.removeFigure(ghost); // OR ghost->removeFromParent();
    }
    ghostVehCount_--;
}
