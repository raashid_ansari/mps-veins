/*
 * Vehicle.cc
 *
 *  Created on: Jun 6, 2017
 *      Author: Raashid Ansari
 */

#include "Car.h"

u_int cars::Car::carCount_         = 0;
u_int cars::Car::normalCarCount_   = 0;
u_int cars::Car::attackerCarCount_ = 0;

cars::Car::Car() {}

cars::Car::Car(std::string car_type):
        carType_(car_type),
        simulationId_(carCount_++) {
    if (this->carType_ == "normal")
        normalCarCount_++;
    else if (this->carType_ == "attacker")
        attackerCarCount_++;
    else
        cRuntimeError("Invalid car type entered!");
    baseTrafficManager_->createVehicle(getGivenId());
}

cars::Car::~Car() {
    carCount_--;
    if (this->carType_ == "normal")
        normalCarCount_--;
    else if (this->carType_ == "attacker")
        attackerCarCount_--;
    else
        cRuntimeError("Invalid car type entered!");
}

const std::string cars::Car::getGivenId() const {
    auto car_name = std::ostringstream{};
    car_name << this->carType_ << this->simulationId_;
    return car_name.str();
}

void cars::Car::setGivenId(std::string car_type, int simulation_id) {
    this->carType_      = car_type;
    this->simulationId_ = simulation_id;
}

const u_int cars::Car::getCarCount(const char car_type) const {
    if (car_type == 'n')
        return normalCarCount_;
    else if (car_type == 'a')
        return attackerCarCount_;
    else
        return carCount_;
}

const int          cars::Car::getSimulationId()                                       const { return this->simulationId_; }
void               cars::Car::setSimulationId(const int simulationId)                       { this->simulationId_ = simulationId; }

const std::string& cars::Car::getCarType()                                            const { return this->carType_; }
void               cars::Car::setCarType(const std::string& carType)                        { this->carType_ = carType; }

cModule*           cars::Car::getSimulationModule()                                   const { return this->simulationModule_; }
void               cars::Car::setSimulationModule(omnetpp::cModule* simulationModule)       { this->simulationModule_ = simulationModule; }
