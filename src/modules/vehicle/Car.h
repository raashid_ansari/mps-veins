/*
 * Vehicle.h
 *
 *  Created on: Jun 6, 2017
 *      Author: Raashid Ansari
 */

#ifndef CAR_H_
#define CAR_H_

#include <string>

#include "traffic/BaseTrafficManager.h"

namespace cars
{
class Car
{
public:
    Car();
    Car(std::string carType);
    virtual ~Car();

    const int          getSimulationId    () const;
    void               setSimulationId    (const int simulationId);

    const std::string& getCarType         () const;
    void               setCarType         (const std::string& carType);

    cModule*           getSimulationModule() const;
    void               setSimulationModule(omnetpp::cModule* simulationModule);

    const u_int        getCarCount        (const char carType = 't') const;

    const std::string  getGivenId         () const;
    void               setGivenId         (const std::string carType, const int simulationId);

    BaseTrafficManager* baseTrafficManager_ = FindModule<BaseTrafficManager*>::findGlobalModule();

protected:
    std::string  carType_;
    int          simulationId_; // from getId()
    cModule*     simulationModule_; // from findHost()
    static u_int carCount_;
    static u_int normalCarCount_;
    static u_int attackerCarCount_;
}; // class Vehicle
} // namespace cars

#endif /* CAR_H_ */
