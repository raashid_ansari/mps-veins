/*
 * GhostVehicle.h
 *
 *  Created on: Jun 6, 2017
 *      Author: veins
 */

#ifndef GHOSTVEHICLE_H_
#define GHOSTVEHICLE_H_

#include <omnetpp/ccanvas.h>
#include <string>

namespace cars
{
class GhostVehicle
{
public:
    GhostVehicle();
    ~GhostVehicle();

    void createVehicle (omnetpp::cCanvas&, const std::string);
    void updatePosition(const omnetpp::cFigure::Point) const;
    void removeVehicle (omnetpp::cCanvas&, std::string);

protected:
    omnetpp::cRectangleFigure* ghostVehFigure_;
    static int                 ghostVehCount_;
}; // class GhostVehicle
} // namespace Cars

#endif /* GHOSTVEHICLE_H_ */
