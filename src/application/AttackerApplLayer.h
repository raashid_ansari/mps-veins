//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __GHOSTCARS_AttackerApplLayer_H_
#define __GHOSTCARS_AttackerApplLayer_H_

#include <omnetpp/ccomponent.h>
#include <algorithm>
#include <map>

#include "CarApplLayer.h"
#include "modules/vehicle/GhostVehicle.h"

/**
 * Application layer for Attacker Car
 */
class AttackerApplLayer : public CarApplLayer
{
public:
    AttackerApplLayer ();
    ~AttackerApplLayer();
    void initialize   (int stage);
    void finish       ();
//    virtual int numInitStages() const { return std::max(cSimpleModule::numInitStages(), 4); }

protected:
    std::map<int, double> ghostVehDistMap_;
    cOutVector            ghostVehDistVec_;
    cOutVector            ghostVelocityVec_;
    cOutVector            attackDurationVec_;
    bool                  constantPositionAttackOffsetFlag_;

    enum Attacks {
        NO_ATTACKS,
        SUDDEN_APPEARANCE,
        SUDDEN_APPEARANCE2,
        BRAKE_COMM_RANGE
    };

protected:
    void handleSelfMsg     (cMessage* msg);
    void onBSMExt          (BasicSafetyMessageExtended& bsm);
    void selectAttack      (const BasicSafetyMessageExtended& bsm);
    void brakeFromCommRange(const BasicSafetyMessageExtended& bsm);
    void suddenAppearance  (const BasicSafetyMessageExtended& bsm);
    void constantPosition  (const BasicSafetyMessageExtended& bsm);
};

#endif
