//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "AttackerApplLayer.h"

Define_Module(AttackerApplLayer);

inline simtime_t getRandTime() {
    return fmod(std::rand(), 2.0) + 1;
}

inline double toPositiveAngle(double angle) {
   angle = fmod(angle, 360);
   while (angle < 0) angle += 360.0;
   return angle;
}

AttackerApplLayer::AttackerApplLayer() {}
AttackerApplLayer::~AttackerApplLayer() {}

void AttackerApplLayer::initialize(int stage) {
    // std::cout << "AttackerApplLayer: STAGE " << stage << " init start" << std::endl;
    CarApplLayer::initialize(stage);
    if (stage == 0) {
        ghostVehDistVec_.setName("ghostVehicleDistance");
        ghostVelocityVec_.setName("ghostVehicleVelocity");
        attackDurationVec_.setName("attack");
        constantPositionAttackOffsetFlag_ = true;
        // std::cout << "AttackerApplLayer: STAGE 0 init done" << std::endl;
    }

    if (stage == 1) {
        thisCar_ = *(carVec_.rbegin());
        thisCar_->setSimulationId(findHost()->getId());
        thisCar_->setSimulationModule(findHost());
        thisCar_->getSimulationModule()->setName(thisCar_->getGivenId().c_str());
        for (auto i = 0; i < trafficManager_->getGhostCarPerAttacker(); ++i)
            ghostVehDistMap_.insert(std::make_pair(i, connManager_->getInterfDist()));
////        For some reason, getId() gives ID of the module + 1, hence need to deduct one to counter that.
//         std::cout << "ID of this module is: " << omnetpp::cSimpleModule::getId() - 1 << std::endl;
//         std::cout << "AttackerApplLayer: STAGE 1 init done" << std::endl;
    }
}

void AttackerApplLayer::handleSelfMsg(cMessage* msg) {
    CarApplLayer::handleSelfMsg(msg);
    return;
}

void AttackerApplLayer::onBSMExt(BasicSafetyMessageExtended& bsm) {
    if (hasGUI() && !getEnvir()->isExpressMode())
        Visuals(thisCar_).showBSMBubble(bsm.getSenderAddress());
    if (par("attackerType").longValue() != NO_ATTACKS) // attack iff they should be performed
        selectAttack(bsm);
    return;
}

void AttackerApplLayer::selectAttack(const BasicSafetyMessageExtended& bsm) {
    const auto ATTACK_FROM = par("attackFrom").longValue();
    const auto ATTACK_UNTIL = par("attackUntil").longValue();
    const auto ATTACKER_TYPE = par("attackerType").longValue();
    const auto GHOST_STRING = std::string("ghost").c_str();

    if (simTime() > ATTACK_FROM && simTime() <= ATTACK_UNTIL && bsm.getWsmData() != GHOST_STRING) {
        attackDurationVec_.recordWithTimestamp(simTime(), 70.0); // Record attack duration
        switch (ATTACKER_TYPE) {
            case SUDDEN_APPEARANCE:
                suddenAppearance(bsm);
                break;
            case SUDDEN_APPEARANCE2:
                constantPosition(bsm);
                break;
            case BRAKE_COMM_RANGE:
                brakeFromCommRange(bsm);
                break;
            default:
                error("No attacker type defined for attackerType_ index: \%s\\", ATTACKER_TYPE);
                break;
        }
    } else {
        if (hasGUI() && !getEnvir()->isExpressMode()) {
            Visuals(thisCar_).updateRing(connManager_->getInterfDist(), "-");
            if (simTime() > ATTACK_UNTIL) { // Delete ghost figure
                auto ss = std::stringstream{};
                ss << "ghost_" << thisCar_->getSimulationId() << "_" << bsm.getSenderAddress();
                auto ghost = cars::GhostVehicle{};
                ghost.removeVehicle(*canvas_, ss.str());
            }
        }
    }
}

void AttackerApplLayer::suddenAppearance(const BasicSafetyMessageExtended& bsm) {
    std::srand(std::time(0));
    for (int i = 0; i < trafficManager_->getGhostCarPerAttacker(); ++i) {
        auto spoofBsm = new BasicSafetyMessageExtended(); // make this global variable so as to delete it in finish()
        spoofBsm->setWsmVersion(1);
        spoofBsm->setTimestamp(simTime());
//        spoof_bsm->setSenderAddress(std::rand() % 100); // random ID being sent in spoofed BSM
        spoofBsm->setSenderAddress(50+i);
        spoofBsm->setRecipientAddress(0); // was "rcvId" that is defaulted to 0 in BaseWaveApplLayer
        spoofBsm->setSerial(0);           // was "serial", defaulted to 0 in BaseWaveApplLayer
        spoofBsm->setBitLength(headerLength);
        spoofBsm->setPsid(-1);
        spoofBsm->setChannelNumber(Channels::CCH);
        spoofBsm->addBitLength(beaconLengthBits);
        spoofBsm->setPriority(beaconPriority);
        spoofBsm->setWsmData("ghost");

        auto targetSd = getSafetyDistance(bsm.getSenderSpeed()); // get target's safety distance
        auto ghostVehPos = targetSd - 0.1; // put ghost just within target's safety distance
        auto posOffset = getPosOffset(
                traciMobility_->getCurrentPosition(),
                bsm.getSenderPos(),
                ghostVehPos
                ); // calculate ghost vehicle's position w.r.t. target's safety distance
        auto ghostVehDist = bsm.getSenderPos().distance(posOffset);
        ghostVehDistVec_.record(ghostVehDist);
        spoofBsm->setSenderPos(posOffset - Coord(i,i,0));

        auto spoofVel = Coord(); // creates a speed object with speed = 0
        auto spoofVelDouble = std::sqrt(std::pow(spoofVel.x, 2) + std::pow(spoofVel.y, 2) + std::pow(spoofVel.z, 2));
        ghostVelocityVec_.record(spoofVelDouble);
        spoofBsm->setSenderSpeed(spoofVel);

        if (hasGUI() && !getEnvir()->isExpressMode()) {
            Visuals(thisCar_).updateRing(connManager_->getInterfDist(), "yellow");
            Visuals(thisCar_).updateGhostVisuals(bsm, *spoofBsm, *canvas_);
        }

        sendDown(spoofBsm);
    }
}

void AttackerApplLayer::constantPosition(const BasicSafetyMessageExtended& bsm) {
    std::srand(std::time(0));
    auto ghostPos = Coord();
    if (constantPositionAttackOffsetFlag_) { // This flag is used to stop code from updating ghost position
        constantPositionAttackOffsetFlag_ = false;
        auto ghostPosOffset = par("ghostPosOffset").doubleValue();

//      calculate ghost vehicle's position w.r.t. target's safety distance
        ghostPos = getPosOffset(traciMobility_->getCurrentPosition(), bsm.getSenderPos(), ghostPosOffset);
    }

    for (int i = 0; i < trafficManager_->getGhostCarPerAttacker(); ++i) {
        auto spoofBsm = new BasicSafetyMessageExtended(); // TODO: make this global variable so as to delete it in finish()
        spoofBsm->setWsmVersion(1);
        spoofBsm->setTimestamp(simTime());
//        spoof_bsm->setSenderAddress(std::rand() % 100); // random ID sent in
                                                          // spoofed BSM
        spoofBsm->setSenderAddress(50+i);
        spoofBsm->setRecipientAddress(0); // was "rcvId" that is defaulted to
                                           // 0 in BaseWaveApplLayer
        spoofBsm->setSerial(0); // was "serial", defaulted to 0 in
                                 // BaseWaveApplLayer
        spoofBsm->setBitLength(headerLength);
        spoofBsm->setPsid(-1);
        spoofBsm->setChannelNumber(Channels::CCH);
        spoofBsm->addBitLength(beaconLengthBits);
        spoofBsm->setPriority(beaconPriority);
        spoofBsm->setWsmData("ghost");

        auto ghostVehDist = bsm.getSenderPos().distance(ghostPos);
        ghostVehDistVec_.record(ghostVehDist);
        spoofBsm->setSenderPos(ghostPos - Coord(i,i,0));

        auto spoofVel = Coord(); // creates a speed object with speed = 0
        auto spoofVelDouble = std::sqrt(std::pow(spoofVel.x, 2) + std::pow(spoofVel.y, 2) + std::pow(spoofVel.z, 2));
        ghostVelocityVec_.record(spoofVelDouble);
        spoofBsm->setSenderSpeed(spoofVel);

        // update simulation visuals
        if (hasGUI() && !getEnvir()->isExpressMode()) {
            auto visuals = Visuals(thisCar_);
            visuals.updateRing(connManager_->getInterfDist(), "yellow");
            visuals.updateGhostVisuals(bsm, *spoofBsm, *canvas_);
        }

        sendDown(spoofBsm);
    }
}

void AttackerApplLayer::brakeFromCommRange(const BasicSafetyMessageExtended& bsm) {
    std::srand(std::time(0));
//    double targetCR = conn_manager_->getInterfDist();
    for (auto i = 0; i < trafficManager_->getGhostCarPerAttacker(); ++i) {
        auto spoofBsm = new BasicSafetyMessageExtended(); // make this global variable so as to delete it in finish()
        spoofBsm->setWsmVersion(1);
        spoofBsm->setTimestamp(simTime());
//        spoof_bsm->setSenderAddress(std::rand() % 100); // was "myId"
        spoofBsm->setSenderAddress(50+i); // was "myId"
        spoofBsm->setRecipientAddress(0); // was "rcvId" that is defaulted to 0 in BaseWaveApplLayer
        spoofBsm->setSerial(0); // was "serial", defaulted to 0 in BaseWaveApplLayer
        spoofBsm->setBitLength(headerLength);
        spoofBsm->setPsid(-1);
        spoofBsm->setChannelNumber(Channels::CCH);
        spoofBsm->addBitLength(beaconLengthBits);
        spoofBsm->setPriority(beaconPriority);
        spoofBsm->setWsmData("ghost");

        auto posOffset = getPosOffset(
                traciMobility_->getCurrentPosition(),
                bsm.getSenderPos(),
                ghostVehDistMap_.find(i)->second
                ); // Get offset for vehicle position
        auto ghostVehPos = posOffset - Coord(i, i, 0);
        spoofBsm->setSenderPos(ghostVehPos);

        auto ghostVehDist = bsm.getSenderPos().distance(posOffset);
        ghostVehDistVec_.record(ghostVehDist);

        auto spoofVel = Coord(traciMobility_->getCurrentSpeed());
        auto spoofVelDouble = std::sqrt(std::pow(spoofVel.x, 2) + std::pow(spoofVel.y, 2) + std::pow(spoofVel.z, 2));
        ghostVelocityVec_.record(spoofVelDouble);
        spoofBsm->setSenderSpeed(spoofVel);

        // Update simulation visuals
        if (hasGUI() && !getEnvir()->isExpressMode()) {
            auto visuals = Visuals(thisCar_);
            visuals.updateRing(connManager_->getInterfDist(), "yellow");
            visuals.updateGhostVisuals(bsm, *spoofBsm, *canvas_);
        }

        sendDown(spoofBsm);

        // keep ghost vehicle from going behind target vehicle
        if (ghostVehDistMap_.find(i)->second > 5) // TODO: Change this to if ghost distance is less than 5 then throw exception
            ghostVehDistMap_.find(i)->second -= 5;
    }
}

void AttackerApplLayer::finish() {
    std::cout << "AttackerApplLayer: finish() for " << findHost()->getFullName() << std::endl;
}
