//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __GHOSTCARS_CarApplLayer_H_
#define __GHOSTCARS_CarApplLayer_H_

#include <omnetpp/ccomponent.h>

#include "veins/modules/application/ieee80211p/BaseWaveApplLayer.h"
#include "veins/modules/mobility/traci/TraCICommandInterface.h"
#include "veins/base/connectionManager/ConnectionManager.h"
#include "traffic/TrafficManager.h"
#include "modules/utility/Visuals.h"
#include "modules/utility/FileLogger.h"

/**
 * Application layer for a Car
 */
class CarApplLayer : public BaseWaveApplLayer
{
public:
    CarApplLayer   ();
    ~CarApplLayer  ();
//     virtual int numInitStages() const { return std::max(cSimpleModule::numInitStages(), 4); }
    void initialize(int stage);
    void finish    ();

protected:
    typedef std::vector<cars::Car*> CarVector_t;
    typedef std::vector<Coord>      CoordVector_t;
    typedef std::vector<double>     DoubleVector_t;

    Veins::TraCIMobility*                  traciMobility_;
    Veins::TraCICommandInterface*          traciCmdInterface_;
    Veins::TraCICommandInterface::Vehicle* traciVehCmdInterface_;
    Veins::AnnotationManager*              annotations_;
    Veins::TraCIScenarioManager*           traciScenarioManager_;
    TrafficManager*                        trafficManager_;
    BaseTrafficManager*                    baseTrafficManager_;
    ConnectionManager*                     connManager_;
    cCanvas*                               canvas_;
    CarVector_t                            carVec_;
    cars::Car*                             thisCar_;

protected:
    void         handleSelfMsg             (cMessage* msg);
    void         onBSM                     (BasicSafetyMessageExtended* bsm);
//    virtual void getActiveCars             ();
    const double getSafetyDistance         (const Coord speedCoords) const;
    Coord        getMovingStandardDeviation(CoordVector_t& attribute);
    double       getMovingStandardDeviation(DoubleVector_t& attribute);

    /* @brief Find if car is ahead or behind */
    const bool   isBehind                  (const Coord currPos, const Coord senderPos, const Coord currDirection) const;
    Coord        getPosOffset              (Coord attackerPos, Coord targetPos, double distance);
};

#endif
