//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <algorithm>

#include "CarApplLayer.h"

Define_Module(CarApplLayer);

inline simtime_t getRandTime() {
    return fmod(std::rand(), 2.0) + 1;
}

inline double toPositiveAngle(double angle) {
   angle = fmod(angle, 360);
   while (angle < 0) angle += 360.0;
   return angle;
}

CarApplLayer::CarApplLayer() {}
CarApplLayer::~CarApplLayer() {}

void CarApplLayer::initialize(int stage) {
    // std::cout << "CarApplLayer: STAGE " << stage << " init start" << std::endl;
    BaseWaveApplLayer::initialize(stage);
    switch (stage) {
        case 0: {
            traciMobility_ = Veins::TraCIMobilityAccess().get(getParentModule());
            traciCmdInterface_ = traciMobility_->getCommandInterface();
            traciVehCmdInterface_ = traciMobility_->getVehicleCommandInterface();
            annotations_ = Veins::AnnotationManagerAccess().getIfExists();
            connManager_ = FindModule<ConnectionManager*>::findGlobalModule();
            canvas_ = getParentModule()->getParentModule()->getCanvas();
            // std::cout << "CarApplLayer: STAGE 0 init done" << std::endl;
            break;
        }
        case 1: {
            traciScenarioManager_ = FindModule<Veins::TraCIScenarioManager*>::findGlobalModule();
            trafficManager_ = FindModule<TrafficManager*>::findGlobalModule();
            baseTrafficManager_ = new BaseTrafficManager();
            carVec_ = trafficManager_->getCarVector();
    //        For some reason, getId() gives ID of the module + 1, hence need to deduct one to counter that.
    //        // std::cout << "ID of this module is: " << omnetpp::cSimpleModule::getId() - 1 << std::endl;
            // std::cout << "CarApplLayer: STAGE 1 init done" << std::endl;
            break;
        }
    }
}

void CarApplLayer::handleSelfMsg(cMessage *msg) {
    BaseWaveApplLayer::handleSelfMsg(msg);
}

void CarApplLayer::onBSM(BasicSafetyMessageExtended *bsm) {
    // std::cout << "CarApplLayer: Inside onBSM" << std::endl;
//    // std::cout << "BSM from " << bsm->getSenderModule()->getParentModule()->getParentModule()->getFullName() << "\t"
//              << " Sender address: " << bsm->getSenderAddress() << "\t"
//              << " Speed: " << bsm->getSenderSpeed() << "\t\t"
//              << " Position: " << bsm->getSenderPos() << "\t\t"
//              << " Distance: " << mobility_->getCurrentPosition().distance(bsm->getSenderPos()) << "\t\t"
//              << " BSM Data: " << bsm->getWsmData() << "\t\t"
//              << std::endl;
//     std::cout << "CarApplLayer: onBSM done" << std::endl;
}

const bool CarApplLayer::isBehind(const Coord position, const Coord senderPos, const Coord direction) const { // TODO: Probably use omnet2traci class for this or traci2omnet class
    auto angle = toPositiveAngle(std::atan2(direction.y, direction.x) * 180 / M_PI);
//    // std::cout << "My Direction: " << direction << "\t\t"
//            << "Direction (degrees): "  << angle << "\t"
//            << std::endl;

//======================================================================================================================
//    Given a directed line from point p0(x0, y0) to p1(x1, y1),
//    the following condition tells whether a point p2(x2, y2) is on the left of the line, on the right, or on the same
//    line:
//    value = (x1 - x0)*(y2 - y0) - (y1 - y0)*(x2 - x0)
//
//    if value > 0, p2 is on the left side of the line.
//    if value = 0, p2 is on the same line.
//    if value < 0, p2 is on the right side of the line.
//    https://math.stackexchange.com/questions/175896/finding-a-point-along-a-line-a-certain-distance-away-from-another-point
//======================================================================================================================

    if (angle > 0 && angle < 180) {
//        // std::cout << "Going Downwards or Rightwards" << std::endl;
        // choose point on positive y axis
        Coord p1 = Coord(0, position.y, position.z);
        return (p1.x - position.x) * (senderPos.y - position.y) - (p1.y - position.y) * (senderPos.x - position.x) > 0;
    } else if (angle > 180 && angle < 360) {
        // // std::cout << "Going Upwards or Leftwards" << std::endl;
        // choose point on negative y axis
        Coord p1 = Coord(0, position.y, position.z);
        return (p1.x - position.x) * (senderPos.y - position.y) - (p1.y - position.y) * (senderPos.x - position.x) < 0;
    } else if (angle == 180) {
        // // std::cout << "On 180 line" << std::endl;
        // choose point perpendicularly above x axis
        Coord p1 = Coord(position.x, position.y+5, position.z);
        return (p1.x - position.x) * (senderPos.y - position.y) - (p1.y - position.y) * (senderPos.x - position.x) < 0;
    } else if (angle == 0 || angle == 360) {
        // // std::cout << "On 0 line" << std::endl;
        // choose point perpendicularly below x axis
        Coord p1 = Coord(position.x, position.y+5, position.z);
        return (p1.x - position.x) * (senderPos.y - position.y) - (p1.y - position.y) * (senderPos.x - position.x) > 0;
    } else {
        error("Invalid angle given to isBehind function");
    }
    return -1;
}

Coord CarApplLayer::getPosOffset(Coord attackerPos, Coord targetPos, double distance) {
    auto tmp = targetPos - attackerPos;
    auto u = tmp / std::sqrt(std::pow(tmp.x, 2) + std::pow(tmp.y, 2) + std::pow(tmp.z, 2));
    tmp.x = targetPos.x + distance * u.x;
    tmp.y = targetPos.y + distance * u.y;
    tmp.z = targetPos.z + distance * u.z;
    return tmp; // equivalent to return attacker_pos + (distance + attacker_pos.distance(target_pos)) * u;
}

const double CarApplLayer::getSafetyDistance(const Coord speedCoords) const {
    const auto MIN_SAFETY_DIST = 5.0;
    auto speed = std::sqrt(std::pow(speedCoords.x, 2) + std::pow(speedCoords.y, 2));
    // return calculated safety distance formula based on the 3-second rule as explained in the Wikipedia article at:
    // https://en.wikipedia.org/wiki/Assured_Clear_Distance_Ahead#Seconds_of_distance_to_stop_rule_2
    // safetyDist = (speed^2/20) + speed
    auto calcSafetyDist = (std::pow(speed, 2) / 20) + speed;
    return calcSafetyDist < MIN_SAFETY_DIST ? MIN_SAFETY_DIST : calcSafetyDist;
}

Coord CarApplLayer::getMovingStandardDeviation(CoordVector_t& attribute) {

    // Calculate mean
    auto sum = Coord();
    for (auto& a : attribute) {
        sum += a;
    }
    auto mean = Coord();
    mean = sum/attribute.size();

    // Calculate difference from mean and add it all up
    auto diffFromMean = Coord();
    for (auto& a : attribute) {
        diffFromMean.x += std::pow(a.x - mean.x, 2);
        diffFromMean.y += std::pow(a.y - mean.y, 2);
        diffFromMean.z += std::pow(a.z - mean.z, 2);
    }

    // Calculate variance
    auto variance = Coord();
    variance.x = diffFromMean.x/attribute.size();
    variance.y = diffFromMean.y/attribute.size();
    variance.z = diffFromMean.z/attribute.size();

    // Calculate standard deviation
    auto stdDev = Coord();
    stdDev.x = std::sqrt(variance.x);
    stdDev.y = std::sqrt(variance.y);
    stdDev.z = std::sqrt(variance.z);

    return stdDev;
}

double CarApplLayer::getMovingStandardDeviation(DoubleVector_t& attribute) {
    // Calculate mean
    auto sum = std::accumulate(attribute.begin(), attribute.end(), 0.0);
    auto mean = sum/attribute.size();

    // Calculate difference from mean and add it all up
    auto diffFromMean = 0.0;
    for (auto& a : attribute)
        diffFromMean += std::pow(a - mean, 2);

    // Calculate variance
    auto variance = diffFromMean/attribute.size();

    // return standard deviation
    return std::sqrt(variance);
}

void CarApplLayer::finish() {
    std::cout << "CarApplLayer: finish()" << std::endl;
}
