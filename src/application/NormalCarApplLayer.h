//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __GHOSTCARS_NormalCarApplLayer_H_
#define __GHOSTCARS_NormalCarApplLayer_H_

#include <omnetpp/ccomponent.h>
#include <algorithm>
#include <map>
#include <random>
#include <Eigen/Dense>

#include "CarApplLayer.h"
#include "modules/predictors/KFilter.h"
#include "modules/predictors/EKFilter.h"
#include "modules/predictors/UKFilter.h"
#include "modules/vehicle/GhostVehicle.h"
#include "modules/utility/Parameters.h"
#include "support/PlausibilityChecks.h"

/**
 * Application layer for Normal Car
 */

class NormalCarApplLayer : public CarApplLayer
{
public:
    NormalCarApplLayer ();
    ~NormalCarApplLayer();
    void initialize    (int stage);
    void finish        ();
//    virtual int numInitStages() const { return std::max(cSimpleModule::numInitStages(), 4); }

protected:
    typedef std::set<int>                IntSet_t;
    typedef std::vector<Eigen::VectorXd> EigenVectorXdVector_t;

    simtime_t          lastBrakeAt_;
    std::random_device rndSeed_;
    std::mt19937       generator_;
    bool               brakeVar_;
    bool               isDetectionOn_;
    int                predictor_;

    mutable cOutVector safetyDistVec_;
    mutable cOutVector commRangeVec_;
    mutable cOutVector brakeApplyVec_;
    mutable cOutVector speedUpVec_;
    mutable cOutVector attackDetectedVec_;

    struct KFTrackerAndPredictions {
        KFilter               kfTracker_;
        EigenVectorXdVector_t predictions_;
    };

    struct EKFTrackerAndPredictions {
        EKFilter              ekfTracker_;
        EigenVectorXdVector_t predictions_;
    };

    struct UKFTrackerAndPredictions {
        UKFilter               ukfTracker_;
        EigenVectorXdVector_t  predictions_;
    };

    mutable std::map<int, KFTrackerAndPredictions>  kfTrackerPredictions_;
    mutable std::map<int, EKFTrackerAndPredictions> ekfTrackerPredictions_;
    mutable std::map<int, UKFTrackerAndPredictions> ukfTrackerPredictions_;
    mutable std::map<int, CoordVector_t>            senderPositionTrace_;
    mutable std::map<int, DoubleVector_t>           senderDistanceTrace_;
    mutable std::map<int, Coord>                    senderPosStdDev_;

    enum Predictor {
        KF,
        EKF,
        UKF
    };

    const int sizeN = 4;
    const int sizeU = 2;
    const int sizeM = 4;
    const int sizeV = sizeM;

    cMessage* mdmCheck_;
    int       mdmCheckPeriod_;

protected:
    void brake                          ();
    void speedUp                        ();
    void handleSelfMsg                  (cMessage* msg);
    void onBSMExt                       (BasicSafetyMessageExtended* bsm);
    void reactToBSM                     (const BasicSafetyMessageExtended* bsm);
    void makePredictions                (const Sender_t& sender);
    void makePredictionsWithEKF         (const Sender_t& sender);
    void makePredictionsWithUKF         (const Sender_t& sender);
    void detectMisbehavior              (BasicSafetyMessageExtended* bsm);
    bool isEstimateFarFromMeasurement   (const Sender_t& sender);
    bool isEstimateFarFromMeasurementEKF(const Sender_t& sender);
    bool isEstimateFarFromMeasurementUKF(const Sender_t& sender);
    bool isDanger                       (const Coord senderPos) const;
    void createKFTracker                (const Parameters& parameters, const int senderId) const;
    void createEKFTracker               (const Parameters parameters, const int senderId) const;
    void createUKFTracker               (const Parameters parameters, const int senderId) const;
};

#endif
