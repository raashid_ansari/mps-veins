//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "NormalCarApplLayer.h"

Define_Module(NormalCarApplLayer);

std::vector<Sender_t> gSenderTracker;



inline simtime_t getRandTime() {
    return fmod(std::rand(), 2.0) + 1;
}



inline double toPositiveAngle(double angle) {
   angle = fmod(angle, 360);
   while (angle < 0) angle += 360.0;
   return angle;
}



NormalCarApplLayer::NormalCarApplLayer() {}
NormalCarApplLayer::~NormalCarApplLayer() {}



void NormalCarApplLayer::
initialize(int stage) {
    // std::cout << "NormalCarApplLayer: STAGE " << stage << " init start" << std::endl;
    CarApplLayer::initialize(stage);

    if (stage == 0) {
        brakeVar_ = true;

        safetyDistVec_.setName    ("safetyDistance");
        safetyDistVec_.setUnit    ("m");

        commRangeVec_.setName     ("communicationRange");
        commRangeVec_.setUnit     ("m");

        brakeApplyVec_.setName    ("brakesApplied");
        speedUpVec_.setName       ("speedUps");
        attackDetectedVec_.setName("attackDetected");

        isDetectionOn_ = par("detection").boolValue();
        predictor_     = par("predictor").longValue();
        // std::cout << "NormalCarApplLayer: STAGE 0 init done" << std::endl;
    }

    if (stage == 1) {
        thisCar_ = *(carVec_.rbegin());
        thisCar_->setSimulationId(findHost()->getId());
        thisCar_->setSimulationModule(findHost());
        thisCar_->getSimulationModule()->setName(thisCar_->getGivenId().c_str());
//        For some reason, getId() gives ID of the module + 1, hence need to deduct one to counter that.
//        std::cout << "ID of this module is: " << omnetpp::cSimpleModule::getId() - 1 << std::endl;
//        std::cout << "NormalCarApplLayer: STAGE 1 init done" << std::endl;

        // setup timer to check MDM periodically
        mdmCheck_ = new cMessage("mdmCheck");
        mdmCheckPeriod_ = par("mdmCheckPeriod").longValue();

        scheduleAt(simTime() + mdmCheckPeriod_, mdmCheck_);
    }
}



void NormalCarApplLayer::
finish() {
    // *************************************
    // LOGGING
    // *************************************
    // Record all sender plausibility check ratings
    // File name format: senderId_receiverId_plausibilityData.dat
    auto senderLogFileName = std::ostringstream{};
    auto logStr = std::ostringstream{};
    auto fileLogger = FileLogger();

    for (auto& sender : gSenderTracker) {
        logStr << "ART MDM EstVsMeasure Suspicion\n"
               << sender.artCheckRating
               << " " << sender.mdmCheckRating
               << " " << sender.estimateVsMeasureRating
               << " " << sender.suspicionRating
               ;
        senderLogFileName << sender.id << "_" << thisCar_->getSimulationId() << "_plausibilityData";
        fileLogger.log(senderLogFileName.str(), logStr.str());

        senderLogFileName.str(std::string()); // clear file name stream
        logStr.str(std::string()); // clear logging stream
    }

    if (mdmCheck_->isScheduled())
        cancelAndDelete(mdmCheck_);

    std::cout << "NormalCarApplLayer: finish() for " << findHost()->getFullName() << std::endl;
}



void NormalCarApplLayer::
handleSelfMsg(cMessage* msg) {
    if (msg == mdmCheck_) {
        // Perform MDM check for each sender in gSenderTracker
        if (!gSenderTracker.empty()) {
            auto plausibilityChecks = PlausibilityChecks();
            plausibilityChecks.minimumDistanceMovedCheck(
                    gSenderTracker,
                    par("beaconTimeThreshold").doubleValue(),
                    connManager_->getInterfDist(),
                    static_cast<int>(par("mdmThreshMultiplier").longValue()));
        }

        // Schedule MDM check again
        scheduleAt(simTime() + mdmCheckPeriod_, mdmCheck_);
    } else {
        CarApplLayer::handleSelfMsg(msg);
    }
}



void NormalCarApplLayer::
onBSMExt(BasicSafetyMessageExtended* bsm) {
    if (hasGUI() && !getEnvir()->isExpressMode()) {
        auto visuals = Visuals(thisCar_);
        visuals.showBSMBubble(bsm->getSenderAddress());
    }
    detectMisbehavior(bsm); // change brake variable in this function
    reactToBSM(bsm);
}



void NormalCarApplLayer::
reactToBSM(const BasicSafetyMessageExtended* bsm) {
    auto senderPos = bsm->getSenderPos();
    isDanger(senderPos) ? brake() : speedUp();
}



void NormalCarApplLayer::
detectMisbehavior(BasicSafetyMessageExtended* bsm) {
    auto window = static_cast<u_int>(par("movingWindow").longValue());
    auto sender = Sender_t{};

    sender.id                      = bsm->getSenderAddress();
    sender.speed                   = bsm->getSenderSpeed();
    sender.position                = bsm->getSenderPos();
    sender.isDiscarded             = false;
    sender.estimateVsMeasureRating = 0;
    sender.artCheckRating          = 0;
    sender.mdmCheckRating          = 0;
    sender.suspicionRating         = 0;
    sender.acceleration            = bsm->getSenderAcceleration();
    sender.lastBeaconTime          = bsm->getSendingTime();

    auto isFound = false;

    // check if new ID received
    for (auto& s : gSenderTracker) {
        if (sender.id == s.id) {
            isFound = true;
            break;
        }
    }

    if (!isFound) {
        // record first received position from a new sender.
        // this will be used in MDM check
        sender.firstPos = sender.position;

        // initialize sender's position trace vector
        auto senderPositionInit = CoordVector_t(window);
        senderPositionInit.at(window-1) = sender.position;
        senderPositionTrace_.insert(std::make_pair(sender.id, senderPositionInit));

        //initialize sender's distance trace vector
        auto senderDistanceInit = std::vector<double>(window);
        *senderDistanceInit.rbegin() = mobility_->getCurrentPosition().distance(sender.position);
        senderDistanceTrace_.insert(std::make_pair(sender.id, senderDistanceInit));

        // initialize sender's positional standard deviation value
        senderPosStdDev_.insert(std::make_pair(sender.id, Coord()));

        // initialize predictor
        sender.acceleration = bsm->getSenderAcceleration();
        auto parameters = Parameters(sender.position, sender.speed, sender.acceleration);
        switch (predictor_) {
            case KF:
                createKFTracker(parameters, sender.id);
                break;
            case EKF:
                createEKFTracker(parameters, sender.id);
                break;
            case UKF:
                createUKFTracker(parameters, sender.id);
                break;
            default:
                error("Invalid value selected for tracker");
                break;
        }
        gSenderTracker.push_back(sender);
    }

    // *********************************
    // Plausibility Checks
    // *********************************
    auto plausibilityChecks = PlausibilityChecks();

    // Acceptance Range Threshold Check
    auto artCheck = plausibilityChecks.acceptanceRangeThresholdCheck(
            sender.position,
            traciMobility_->getCurrentPosition(),
            connManager_->getInterfDist());

    // If check not passed, tag BSM sender as suspicious
    if (artCheck) {
        for (auto& s : gSenderTracker) // find the sender
            if (s.id == sender.id)
                s.artCheckRating++;
    }

    switch (predictor_) {
        case KF:
            makePredictions(sender);
            break;
        case EKF:
            makePredictionsWithEKF(sender);
            break;
        case UKF:
            makePredictionsWithUKF(sender);
            break;
        default:
            error("Invalid value selected for tracker");
            break;
    }

    bool isEstimateAndMeasureTooFar = false;

    switch (predictor_) {
        case KF:
            isEstimateAndMeasureTooFar = isEstimateFarFromMeasurement(sender);
            break;
        case EKF:
            isEstimateAndMeasureTooFar = isEstimateFarFromMeasurementEKF(sender);
            break;
        case UKF:
            isEstimateAndMeasureTooFar = isEstimateFarFromMeasurementUKF(sender);
            break;
        default:
            error("Invalid value selected for tracker");
            break;
    }

    if (isEstimateAndMeasureTooFar && isDetectionOn_) {
        // record spoof BSM
        for (auto& s : gSenderTracker)
            if (s.id == sender.id)
                s.estimateVsMeasureRating++;

        attackDetectedVec_.record(80.0);
        brakeVar_ = false;
    }

    // retrieve sender's earlier position trace vector
    // discard oldest position, insert newest position
    auto senderPositionTrace = &senderPositionTrace_.find(sender.id)->second;
    std::rotate(senderPositionTrace->begin(), senderPositionTrace->begin() + 1, senderPositionTrace->end());
    *senderPositionTrace->rbegin() = sender.position;

    // retrieve sender's earlier distance trace vector
    // discard oldest distance, insert newest distance
    auto senderDistanceTrace = &senderDistanceTrace_.find(sender.id)->second;
    std::rotate(senderDistanceTrace->begin(), senderDistanceTrace->begin() + 1, senderDistanceTrace->end());
    auto senderDistance = mobility_->getCurrentPosition().distance(sender.position);
    *senderDistanceTrace->rbegin() = senderDistance;

    // get position as SUMO/GPS coordinates instead of Omnet++ coordinates
    auto sumoCoord = traciCmdInterface_->getLonLat(sender.position);

    // update sender's position standard deviation value
    auto senderPositionStandardDeviation = getMovingStandardDeviation(*senderPositionTrace);

    // update sender's distance standard deviation value
    auto senderDistanceStandardDeviation = getMovingStandardDeviation(*senderDistanceTrace);

    // 1. Add up all the ratings to get a total suspicion rating
    // 2. If suspicionRating crosses SUSPICION_THRESHOLD, discard action on that BSM
    // TODO: discard any action on BSM
    // Need some statistical model to decide what should be done when BSM discarded
    for (auto& s : gSenderTracker) {
        if (s.id == sender.id) {
            s.suspicionRating = s.artCheckRating + s.mdmCheckRating + s.estimateVsMeasureRating;
            const int SUSPICION_THRESHOLD = 5;
            if (s.suspicionRating >= SUSPICION_THRESHOLD)
                s.isDiscarded = true;
        }
    }

    // *************************************
    // LOGGING
    // *************************************
    // Record position, distance trace and standard deviation in external file
    // File name format: <sender_id>_<receiver_id>_data.dat
    auto senderLogFileName = std::ostringstream{};
    senderLogFileName << sender.id << "_" << thisCar_->getSimulationId() << "_data";
    auto logStr = std::ostringstream{};
    logStr << sender.position.x
            << " " << sender.position.y
            << " " << sumoCoord.first
            << " " << sumoCoord.second
            << " " << senderPositionStandardDeviation.x
            << " " << senderPositionStandardDeviation.y
            << " " << sender.speed.x
            << " " << sender.speed.y
            << " " << senderDistance
            << " " << senderDistanceStandardDeviation
            ;
    auto fileLogger = FileLogger();
    fileLogger.log(senderLogFileName.str(), logStr.str());
}



void NormalCarApplLayer::
speedUp() {
    if (hasGUI() && !getEnvir()->isExpressMode())
        Visuals(thisCar_).updateRing(connManager_->getInterfDist(), "-"); // restore interference distance annotation
    traciVehCmdInterface_->slowDown(14.0, 3); // Speed up normal vehicle to given speed, in 3 seconds
}



bool NormalCarApplLayer::
isEstimateFarFromMeasurement(const Sender_t& sender) {
    auto trackerPredictor     = &(kfTrackerPredictions_.find(sender.id)->second);
    auto predictions          = trackerPredictor->predictions_;
    auto distanceFromEstimate = DoubleVector_t{};

    if (predictions.size() > 0) {
        for (auto& it : predictions) {
            auto estimatedPosition = Coord(it[0], it[1], 0.0);
            auto estimatedVsMeasuredPosition = estimatedPosition.distance(sender.position);
            distanceFromEstimate.push_back(estimatedVsMeasuredPosition);
        }
    }

    // Record communication range throughout the simulation
    auto communicationRange = connManager_->getInterfDist();
    commRangeVec_.record(communicationRange);

    if (distanceFromEstimate.size() > 0) {
        auto distanceThreshold = par("distanceThreshold").doubleValue();
        for (auto& it : distanceFromEstimate)
            if (it > distanceThreshold)
                return true;
        //    else if (sender_position.distance(sender_last_pos)-sender_last_vel*(simTime()-last_bsm_recv_time)>distance_threshold
        //            && sender_last_pos != Coord()
        //            && sender_last_vel != Coord())
        //        return true;
            else
                return false;
    }
    return false; // if all else detection fails, tell nothing detected. Causes false negatives.
}



bool NormalCarApplLayer::
isEstimateFarFromMeasurementEKF(const Sender_t& sender) {
    auto trackerPredictor     = &(ekfTrackerPredictions_.find(sender.id)->second);
    auto predictions          = trackerPredictor->predictions_;
    auto distanceFromEstimate = std::vector<double>{};
    if (predictions.size() > 0) {
        for (auto& it : predictions) {
            auto estimatedPosition = Coord(it[0], it[1], 0.0);
            auto estimatedVsMeasuredPosition = estimatedPosition.distance(sender.position);
            distanceFromEstimate.push_back(estimatedVsMeasuredPosition);
        }
    }

    // Record communication range throughout the simulation
    auto communicationRange = connManager_->getInterfDist();
    commRangeVec_.record(communicationRange);

    if (distanceFromEstimate.size() > 0) {
        auto distanceThreshold = par("distanceThreshold").doubleValue();
        for (auto& it : distanceFromEstimate)
            if (it > distanceThreshold)
                return true;
        //    else if (sender_position.distance(sender_last_pos)-sender_last_vel*(simTime()-last_bsm_recv_time)>distance_threshold
        //            && sender_last_pos != Coord()
        //            && sender_last_vel != Coord())
        //        return true;
            else
                return false;
    }
    return false; // if all else detection fails, tell nothing detected. Causes false negatives.
}



bool NormalCarApplLayer::
isEstimateFarFromMeasurementUKF(const Sender_t& sender) {
    auto trackerPredictor     = &(ukfTrackerPredictions_.find(sender.id)->second);
    auto predictions          = trackerPredictor->predictions_;
    auto distanceFromEstimate = std::vector<double>{};
    if (predictions.size() > 0) {
        for (auto& it : predictions) {
            auto estimatedPosition           = Coord(it[0], it[1], 0.0);
            auto estimatedVsMeasuredPosition = estimatedPosition.distance(sender.position);
            distanceFromEstimate.push_back(estimatedVsMeasuredPosition);
        }
    }

    // Record communication range throughout the simulation
    auto communicationRange = connManager_->getInterfDist();
    commRangeVec_.record(communicationRange);

    if (distanceFromEstimate.size() > 0) {
        auto distanceThreshold = par("distanceThreshold").doubleValue();
        for (auto& it : distanceFromEstimate)
            if (it > distanceThreshold)
                return true;
        //    else if (sender_position.distance(sender_last_pos)-sender_last_vel*(simTime()-last_bsm_recv_time)>distance_threshold
        //            && sender_last_pos != Coord()
        //            && sender_last_vel != Coord())
        //        return true;
            else
                return false;
    }
    return false; // if all else detection fails, tell nothing detected. Causes false negatives.
}



bool NormalCarApplLayer::
isDanger(const Coord senderPos) const {
    auto position   = traciMobility_->getCurrentPosition();
    auto direction  = traciMobility_->getCurrentDirection();
    auto safetyDist = getSafetyDistance(traciMobility_->getCurrentSpeed());
    safetyDistVec_.record(safetyDist); // Record safety distance data
    // FIXME: Distance is always positive, need negative distances or is it displacement?
    // Workaround: Implemented isBehind() function that helps calculating if a vehicle is ahead or behind
    auto vehDistance     = traciMobility_->getCurrentPosition().distance(senderPos);
    auto isVehicleBehind = isBehind(position, senderPos, direction);
    auto dangerStatus    = vehDistance < safetyDist && !isVehicleBehind;
    return dangerStatus;
}



void NormalCarApplLayer::
createKFTracker(const Parameters& parameters, const int senderId) const {
    const auto PRED_TIME_STEP       = par("predictionTimeStep").doubleValue();
    auto trackerPredictions         = KFTrackerAndPredictions();

    trackerPredictions.kfTracker_   = KFilter();
    trackerPredictions.kfTracker_.init(PRED_TIME_STEP, parameters);
    trackerPredictions.predictions_ = EigenVectorXdVector_t{0};

    kfTrackerPredictions_.insert(std::make_pair(senderId, trackerPredictions));
}



void NormalCarApplLayer::
createEKFTracker(const Parameters parameters, const int senderId) const {
    const auto PRED_TIME_STEP       = par("predictionTimeStep").doubleValue();
    auto trackerPredictions         = EKFTrackerAndPredictions{};

    trackerPredictions.ekfTracker_  = EKFilter();
    trackerPredictions.ekfTracker_.init(PRED_TIME_STEP, parameters);
    trackerPredictions.predictions_ = EigenVectorXdVector_t{0};

    ekfTrackerPredictions_.insert(std::make_pair(senderId, trackerPredictions));
}



void NormalCarApplLayer::
createUKFTracker(const Parameters parameters, const int senderId) const {
    const auto PRED_TIME_STEP       = par("predictionTimeStep").doubleValue();
    auto trackerPredictions         = UKFTrackerAndPredictions{};

    trackerPredictions.ukfTracker_  = UKFilter();
    trackerPredictions.ukfTracker_.init(PRED_TIME_STEP, parameters);
    trackerPredictions.predictions_ = EigenVectorXdVector_t{0};

    ukfTrackerPredictions_.insert(std::make_pair(senderId, trackerPredictions));
}



void NormalCarApplLayer::
makePredictions(const Sender_t& sender) {
    auto trackerPredictions = &(kfTrackerPredictions_.find(sender.id)->second);
    auto tracker            = &(trackerPredictions->kfTracker_); // Get tracker with same ID as that in received BSM
    assert(tracker != nullptr);

    // Update filter with new measurements
    auto measures           = Eigen::VectorXd(sizeM);
    auto measuresNoise      = Eigen::VectorXd(sizeV);
    auto acceleration       = Eigen::VectorXd(sizeU);

//    std::cout << tracker->getP() << "\n====================0======================" << std::endl;
    std::cout << tracker->getK() << "\n====================0======================" << std::endl;

    acceleration  << sender.acceleration.x, sender.acceleration.y;
    measures      << sender.position.x, sender.position.y, sender.speed.x, sender.speed.y;
    measuresNoise << 0.1, 0.1, 0.1, 0.1;
    tracker->step(acceleration, measures, measuresNoise);

//    std::cout << tracker->getP() << "\n====================0======================" << std::endl;
    std::cout << tracker->getK() << "\n====================0======================" << std::endl;

    // Output next predicted state through getState()
    auto predictedPositions = Eigen::VectorXd(2);
    predictedPositions << tracker->getState()[0], tracker->getState()[1];
    trackerPredictions->predictions_ = EigenVectorXdVector_t{predictedPositions}; // Store all predictions

    // **********************************
    // LOGGING
    // **********************************
    // Record estimates of positions
    auto senderLogFileName = std::ostringstream{};
    senderLogFileName << sender.id
                      << "_" << thisCar_->getSimulationId()
                      << "_data";

    auto logStr = std::ostringstream{};
    logStr << simTime().dbl()
           << " " << trackerPredictions->predictions_[0][0]
           << " " << trackerPredictions->predictions_[0][1];

    auto fileLogger = FileLogger();
    fileLogger.log(senderLogFileName.str(), logStr.str());

    // Get future predictions_
    const auto NUM_PREDICTIONS = par("numOfPredictions").longValue();
    for (auto i = 1; i < NUM_PREDICTIONS; ++i) {
        auto prevState = tracker->getState();
        tracker->predict(acceleration.setZero()); // get update using last state of kalman filter

//        std::cout << tracker->getP() << "\n====================" << i << "======================" << std::endl;
        std::cout << tracker->getK() << "\n====================0======================" << std::endl;

        auto futurePredictedPositions = Eigen::VectorXd(2);
        futurePredictedPositions << prevState[0], prevState[1]; // push new position in pred_pos
        trackerPredictions->predictions_.push_back(futurePredictedPositions);
    }

    if (hasGUI() && !getEnvir()->isExpressMode())
        Visuals().plotPredictions(NUM_PREDICTIONS, sender.id, *canvas_, trackerPredictions->predictions_);
}



void NormalCarApplLayer::
makePredictionsWithEKF(const Sender_t& sender) {
    auto trackerPredictions = &(ekfTrackerPredictions_.find(sender.id)->second);
    auto tracker            = &(trackerPredictions->ekfTracker_); // Get tracker with same ID as that in received BSM
    assert(tracker != nullptr);

    // Update filter with new measurements
    auto measures           = Eigen::VectorXd(sizeM);
    auto measuresNoise      = Eigen::VectorXd(sizeV);
    auto acceleration       = Eigen::VectorXd(sizeU);

    acceleration  << sender.acceleration.x, sender.acceleration.y;
    measures      << sender.position.x, sender.position.y, sender.speed.x, sender.speed.y;
    measuresNoise << 0.0, 0.0, 0.0, 0.0;
    tracker->step(acceleration, measures, measuresNoise);

    // Output next predicted state through getState()
    auto predictedPositions = Eigen::VectorXd(2);
    predictedPositions << tracker->getState()[0], tracker->getState()[1];
    trackerPredictions->predictions_ = EigenVectorXdVector_t{predictedPositions}; // Store all predictions

    // Record estimates of positions
    auto senderLogFileName = std::ostringstream{};
    senderLogFileName << sender.id
                      << "_" << thisCar_->getSimulationId()
                      << "_data";
    auto logStr = std::ostringstream{};
    logStr << simTime().dbl()
           << " " << trackerPredictions->predictions_[0][0]
           << " " << trackerPredictions->predictions_[0][1];

    auto fileLogger = FileLogger();
    fileLogger.log(senderLogFileName.str(), logStr.str());

    // Get future predictions_
    const auto NUM_PREDICTIONS = par("numOfPredictions").longValue();
    for (auto i = 1; i < NUM_PREDICTIONS; ++i) {
        auto prevState = tracker->getState();
//        auto u = Eigen::VectorXd(sizeU);
//        u << 0, 0;
//        tracker->predict(u); // get update using last state of kalman filter
        tracker->predict(acceleration);
        auto futurePredictedPositions = Eigen::VectorXd(2);
        futurePredictedPositions << prevState[0], prevState[1]; // push new position in pred_pos
        trackerPredictions->predictions_.push_back(futurePredictedPositions);
    }

    if (hasGUI() && !getEnvir()->isExpressMode())
        Visuals().plotPredictions(NUM_PREDICTIONS, sender.id, *canvas_, trackerPredictions->predictions_);
}



void NormalCarApplLayer::
makePredictionsWithUKF(const Sender_t& sender) {
    auto trackerPredictions = &(ukfTrackerPredictions_.find(sender.id)->second);
    auto tracker            = &(trackerPredictions->ukfTracker_); // Get tracker with same ID as that in received BSM
    assert(tracker != nullptr);

    // Update filter with new measurements
    auto measures           = Eigen::VectorXd(sizeM);
    auto measuresNoise      = Eigen::VectorXd(sizeV);
    auto acceleration       = Eigen::VectorXd(sizeU);

    acceleration  << sender.acceleration.x, sender.acceleration.y;
    measures      << sender.position.x, sender.position.y, sender.speed.x, sender.speed.y;
    measuresNoise << 0.0, 0.0, 0.0, 0.0;
    tracker->step(acceleration, measures, measuresNoise);

    // Output next predicted state through getState()
    auto predictedPositions = Eigen::VectorXd(2);
    predictedPositions << tracker->getState()[0], tracker->getState()[1];
    trackerPredictions->predictions_ = std::vector<Eigen::VectorXd>{predictedPositions}; // Store all predictions

    // Record estimates of positions
    auto senderLogFileName = std::ostringstream{};
    senderLogFileName << sender.id
                      << "_" << thisCar_->getSimulationId()
                      << "_data";

    auto logStr = std::ostringstream{};
    logStr << simTime().dbl()
           << " " << trackerPredictions->predictions_[0][0]
           << " " << trackerPredictions->predictions_[0][1];

    auto fileLogger = FileLogger();
    fileLogger.log(senderLogFileName.str(), logStr.str());

    // Get future predictions_
    const auto NUM_PREDICTIONS = par("numOfPredictions").longValue();
    for (auto i = 1; i < NUM_PREDICTIONS; ++i) {
        auto prevState = tracker->getState();
//        auto u = Eigen::VectorXd(sizeU);
//        u << 0, 0;
//        tracker->predict(u); // get update using last state of kalman filter
        tracker->predict(acceleration);

        auto futurePredictedPositions = Eigen::VectorXd(2);
        futurePredictedPositions << prevState[0], prevState[1]; // push new position in pred_pos
        trackerPredictions->predictions_.push_back(futurePredictedPositions);
    }

    if (hasGUI() && !getEnvir()->isExpressMode())
        Visuals().plotPredictions(NUM_PREDICTIONS, sender.id, *canvas_, trackerPredictions->predictions_);
}



void NormalCarApplLayer::
brake() {
    // ==================================================================================
    // Initiate random number generator
    // Gaussian Distribution: values near the mean are the most likely,
    // standard deviation affects the dispersion of generated values from the mean
    // ==================================================================================
    generator_.seed(rndSeed_());
    auto dist               = std::normal_distribution<>(0.5, 0.5);
    auto safetyDist         = getSafetyDistance(traciMobility_->getCurrentSpeed());
    auto brakingProbability = par("brakingProbability").doubleValue();

    if (brakingProbability > 1 || brakingProbability < 0)
        error("braking probability has to be within 0 and 1, current value is: %d", brakingProbability);

    // if Car is within safety distance, brake with given probability.
    if ((dist(generator_) <= brakingProbability // if braking probability is higher than random number
        && brakeVar_ // if brake variable is true i.e. should the vehicle react
        && brakingProbability != 0.0) // if braking probability is 0 then don't go further
        || brakingProbability == 1.0) { // if braking probability is 1 then always execute following statements
        if (hasGUI() && !getEnvir()->isExpressMode())
            Visuals(thisCar_).updateRing(safetyDist, "red");
        traciVehCmdInterface_->setSpeed(0.0); // Stops the vehicle
        lastBrakeAt_ = simTime();
        brakeApplyVec_.record(50.0);
    }
    else {
        if (hasGUI() && !getEnvir()->isExpressMode())
            Visuals(thisCar_).updateRing(safetyDist, "green");
    }

    if (simTime()-lastBrakeAt_ > 50) {
        speedUp();
        speedUpVec_.record(60.0); // record why 60?
    }

    if (!brakeVar_)
        brakeVar_ = true;
}
